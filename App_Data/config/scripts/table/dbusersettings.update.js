function update(item, user, request) {

    item.userid = user.userId;
    if (item.userid !== user.userId) {
        request.respond(statusCodes.BAD_REQUEST, "Invalid user id");
    } else {
        request.execute();
    }

}
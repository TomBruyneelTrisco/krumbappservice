var Promise = require("es6-promise").Promise;
var uuid = require('node-uuid');
var azure = require('azure');
var qs = require('querystring');
var appSettings = require('mobileservice-config').appSettings;
var fs = require('fs');


module.exports = {
	addServiceBusCommand: function(queue, command, properties) {
        var connectionString = appSettings.AZURE_SERVICEBUS_CONNECTIONSTRING;
	
		var serviceBusService = azure.createServiceBusService(connectionString);
		
		var promise = new Promise(function (resolve, reject) {
		
			var message = {
				body: command,
				customProperties: properties
			};
		
			serviceBusService.sendQueueMessage(queue, message, function(error) {
				if (!error) {
				
					resolve();
				
				}
				else {
				
					//todo, this error has to be logged and corrected
					console.log("addServiceBusCommand error: " + error);
					resolve();
					
				}
				
			});
			
		});
		
		return promise;
	
	},
	addQueueCommand: function(queue, command) {
	
        var accountName = appSettings.STORAGE_ACCOUNT_NAME;
        var accountKey = appSettings.STORAGE_ACCOUNT_ACCESS_KEY;
		
		var promise = new Promise(function (resolve, reject) {
			
			var queueService = azure.createQueueService(accountName, accountKey);
			
			queueService.createMessage(queue, JSON.stringify(command), {}, function(error) {
				
				if (!error) {
				
					resolve();
				
				}
				else {
				
					//todo, this error has to be logged and corrected
					console.log("addQueueCommand error: " + error);
					resolve();
					
				}
				
			});
			
		});
		
		return promise;
	}
};
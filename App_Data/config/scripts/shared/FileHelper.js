﻿var Promise = require("es6-promise").Promise;
var uuid = require('node-uuid');
var azure = require('azure');
var qs = require('querystring');
var appSettings = require('mobileservice-config').appSettings;
var fs = require('fs');

function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}

module.exports = {
    moveAzureFile: function (fromcontainer, blobname, tocontainer, newblobname) {
        
        var accountName = appSettings.STORAGE_ACCOUNT_NAME;
        var accountKey = appSettings.STORAGE_ACCOUNT_ACCESS_KEY;
        var host = accountName + '.blob.core.windows.net';
        
        var uri = "https://" + host + "/" + fromcontainer + "/" + blobname;
        var newuri = "https://" + host + "/" + tocontainer + "/" + newblobname;
        
        var promise = new Promise(function (resolve, reject) {
            var blobService = azure.createBlobService(accountName, accountKey, host);
            
            blobService.startCopyBlob(uri, tocontainer, newblobname, {}, function (err) {
                if (!err) {
                    // file uploaded
                    resolve(newuri);
                } else {
                    console.error(err);
                    reject();
                }
            });
		
        });
        
        return promise;
		
    },
    saveFilePart: function (file, chunk, chunks, blobname, blobcontainer) {
        
        var accountName = appSettings.STORAGE_ACCOUNT_NAME;
        var accountKey = appSettings.STORAGE_ACCOUNT_ACCESS_KEY;
        var host = accountName + '.blob.core.windows.net';
        var containerName = "uploads";
        var blobName = blobname;
        
        if (chunk == 1) {
            blobName = uuid.v4() + "." + file.name.split('.').pop();
        }
        
        var promise = new Promise(function (resolve, reject) {
            
            var blobService = azure.createBlobService(accountName, accountKey, host);
            
            var readstream = fs.createReadStream(file.path);
            var blockid = blobService.getBlockId("chunk", chunk);
            var filesize = getFilesizeInBytes(file.path);
            
            var stream = blobService.createBlockFromStream(blockid, containerName, blobName, readstream, filesize, {}, function (error) {
                
                if (!error) {
                    // block  uploaded
                    //commit
                    
                    if (chunk == chunks) {
                        
                        var blockids = [];
                        for (var i = 1; i <= chunks; i++) {
                            blockids.push(blobService.getBlockId("chunk", i));
                        }
                        
                        blobService.commitBlocks(containerName, blobName, { 'UncommittedBlocks': blockids }, {}, function (error) {
                            
                            if (!error) {
                                // file uploaded
                                resolve(blobName);
                            } else {
                                console.error(error);
                                reject();
                            }
							
                        });
                    }
                    else
                        resolve(blobName);
					
                } else {
                    console.error(error);
                    reject();
                }
			
            });
			
        });
        
        return promise;
    },
    save: function (req, options) {
        var accountName = appSettings.STORAGE_ACCOUNT_NAME;
        var accountKey = appSettings.STORAGE_ACCOUNT_ACCESS_KEY;
        var host = accountName + '.blob.core.windows.net';
        
        var promise = new Promise(function (resolve, reject) {
            
            if (typeof req.files != "undefined") {
                
                var file = req.files[options.field];
                
                if (typeof file != "undefined") {
                    if ((typeof options.dir !== "undefined") && (options.dir !== null)) {
                        // Set the BLOB store container name on the item, which must be lowercase.
                        options.dir = options.dir.toLowerCase();
                        
                        
                        // If it does not already exist, create the container 
                        // with public read access for blobs.        
                        var blobService = azure.createBlobService(accountName, accountKey, host);
                        blobService.createContainerIfNotExists(options.dir, {
                            publicAccessLevel: 'blob'
                        }, function (error) {
                            if (!error) {
                                
                                nfilename = uuid.v4() + "." + file.name.split('.').pop();
                                
                                blobService.createBlockBlobFromLocalFile(options.dir, nfilename, file.path, function (error, result, response) {
                                    if (!error) {
                                        // file uploaded
                                        resolve("https://" + host + "/" + options.dir + "/" + nfilename);
                                    } else {
                                        console.error(error);
                                        reject();
                                    }
                                });

                            } else {
                                console.error(error);
                                reject();
                            }
                        });
                    } else {
                        reject();
                    }
                }
                else {
                    resolve("");
                }
            }
            else {
                resolve("");
            }
        });
        
        return promise;
    },
    getFromUrl: function (imageURL) {
        var accountName = appSettings.STORAGE_ACCOUNT_NAME;
        var accountKey = appSettings.STORAGE_ACCOUNT_ACCESS_KEY;
        var host = accountName + '.blob.core.windows.net';
        var url = require('url');
        var querystring = require('querystring');
        var https = require('https');
        console.log("Download image from " + imageURL);
        var promise = new Promise(function (resolve, reject) {
            var nfilename = uuid.v4();
            var imageurl = url.parse(imageURL);
            var options = {
                host: imageurl.host,
                port: 443,
                path: imageurl.path,
                method: 'GET'
            }
            var imagereq = https.get(options, function (imageres) {
                //imageres.setEncoding('binary');
                
                var status = imageres.statusCode;
                if (status == 200) {
                    var blobService = azure.createBlobService(accountName, accountKey, host);
                    var writestream = blobService.createWriteStreamToBlockBlob(
                        'images',
                            nfilename,
                            { contentType: 'image/jpeg' },
                            function (error, result, response) {
                            if (error) {
                                console.log("Filehelper: getFromUrl failed " + error);
                                reject();
                            } else {
                                resolve("https://" + host + "/images/" + nfilename);
                            }
                        }
                    );
                    imageres.pipe(writestream);
                }
                else {
                    console.log("Download picture from url failed, status " + status);
                    reject();
                }
                
                imageres.on('error', function (e) {
                    console.log("FileHelper getFromUrl get error " + e.message);
                    reject();
                });
            });
        });

        return promise;
    }
};
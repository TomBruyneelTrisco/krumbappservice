﻿var Promise = require("es6-promise").Promise;
var socialconfig = require('./socialapps');

module.exports = {
    checkUserLogin : function (socialmediatype, socialmediaid, token, verifyurl) {
        
        this.accesstoken = "";
        var self = this;
        
        
        if (socialmediatype == "FB") {
            
            return new Promise(function (resolve, reject) {
                
                function checkToken(accesstoken) {
                    var querystring = require('querystring');
                    var https = require('https');
                    
                    var data = querystring.stringify({
                        input_token: token,
                        access_token: accesstoken
                    });
                    
                    var options = {
                        host: socialconfig.socialapps.facebook_oauth_host,
                        port: 443,
                        path: socialconfig.socialapps.facebook_checkaccess_url + "?" + data,
                        method: 'GET'
                    };
                    
                    var req = https.request(options, function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            
                            
                            fbres = JSON.parse(chunk);
                            
                            if (!fbres.data.is_valid) {
                                resolve(false);
                            }
                            else {
                                if ("" + fbres.data.app_id == socialconfig.socialapps.facebook_appid && "" + fbres.data.user_id == socialmediaid) {
                                    resolve(true);
                                }
                                else
                                    resolve(false);
                            }

                        });
                    });
                    
                    req.end();
                }
                
                if (self.accesstoken == "") {
                    
                    
                    var querystring = require('querystring');
                    var https = require('https');
                    
                    var data = querystring.stringify({
                        client_secret: socialconfig.socialapps.facebook_secret,
                        client_id: socialconfig.socialapps.facebook_appid,
                        grant_type: "client_credentials"
                    });
                    
                    var options = {
                        host: socialconfig.socialapps.facebook_oauth_host,
                        port: 443,
                        path: socialconfig.socialapps.facebook_oauth_url + "?" + data,
                        method: 'GET'
                    };
                    
                    var req = https.request(options, function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            
                            self.accesstoken = chunk.substr(13);
                            
                            checkToken(chunk.substr(13));
                        });
                    });
                    
                    req.end();

                }
                else {
                    
                    checkToken(self.accesstoken);
				
                }
			
            });
        }
        else if (socialmediatype == "AK") {
            
            return new Promise(function (resolve, reject) {
                
                console.log("Verifying AK");
                
                var querystring = require('querystring');
                var https = require('https');
                var url = require('url');
                
                verifyurl = "https://services.antwerpen.be/api/user/0.0.1/me";
                
                var urlparsed = url.parse(verifyurl);
                
                var options = {
                    host: urlparsed.host,
                    port: 443,
                    path: urlparsed.pathname,
                    method: 'GET',
                    headers: { "Authorization": "Bearer " + token }
                };
                
                var req = https.request(options, function (res) {
                    res.setEncoding('utf8');
                    
                    var status = res.statusCode;
                    
                    var data = "";
                    
                    res.on('data', function (chunk) {
                        
                        data += chunk;
						
                    });
                    
                    res.on('end', function () {
                        
                        console.log("Data for AK: " + data);
                        console.log("Status for AK: " + status);
                        
                        if (status == 200) {
                            
                            var answer = JSON.parse(data);
                            console.log("Id for AK: " + answer.data.id);
                            
                            if (answer.data.id == socialmediaid)
                                resolve(true);
                            else
                                resolve(false);
                        }
                        else {
                            resolve(false);
                        }
						
                    });
                });
                
                req.end();
			
            });
			
        }
        else if (socialmediatype == "TWIT") {
            
            return new Promise(function (resolve, reject) {
                
                var querystring = require('querystring');
                var https = require('https');
                var url = require('url');
                
                var urlparsed = url.parse(verifyurl);
                
                var options = {
                    host: urlparsed.host,
                    port: 443,
                    path: urlparsed.pathname,
                    method: 'GET',
                    headers: { "Authorization": token }
                };
                
                var req = https.request(options, function (res) {
                    res.setEncoding('utf8');
                    
                    var status = res.statusCode;
                    
                    var data = "";
                    
                    res.on('data', function (chunk) {
                        
                        data += chunk;
						
                    });
                    
                    res.on('end', function () {
                        
                        if (status == 200) {
                            
                            var answer = JSON.parse(data);
                            
                            if (answer.id_str == socialmediaid)
                                resolve(true);
                            else
                                resolve(false);
                        }
                        else {
                            resolve(false);
                        }
						
                    });
                });
                
                req.end();
			
            });
		
        } else if (socialmediatype == "INST") {
            return new Promise(function (resolve, reject) {
                
                console.log("Verifying INST");
                
                var querystring = require('querystring');
                var https = require('https');
                var url = require('url');
                
                verifyurl = "https://api.instagram.com/v1/users/self/?access_token=" + token;
                
                var urlparsed = url.parse(verifyurl);
                
                var options = {
                    host: urlparsed.host,
                    port: 443,
                    path: urlparsed.path,
                    method: 'GET',
                };
                
                var req = https.request(options, function (res) {
                    res.setEncoding('utf8');
                    
                    var status = res.statusCode;
                    
                    var data = "";
                    
                    res.on('data', function (chunk) {
                        
                        data += chunk;
						
                    });
                    
                    res.on('end', function () {
                        
                        if (status == 200) {
                            
                            var answer = JSON.parse(data);
                            console.log("Id for INST: " + answer.data.id);
                            
                            if (answer.data.id == socialmediaid)
                                resolve(true);
                            else
                                resolve(false);
                        }
                        else {
                            resolve(false);
                        }
						
                    });
                });
                
                req.end();
			
            });
        }
        else {
            return Promise.reject("not a valid social media type");
        }
    },
    downloadRawInstagramMedia: function (verifyurl) {
        return new Promise(function (resolve, reject) {
            var querystring = require('querystring');
            var https = require('https');
            var url = require('url');
            
            var urlparsed = url.parse(verifyurl);
            
            var options = {
                host: urlparsed.host,
                port: 443,
                path: urlparsed.path,
                method: 'GET',
            };
            
            var req = https.request(options, function (res) {
                res.setEncoding('utf8');
                
                var status = res.statusCode;
                
                var data = "";
                
                res.on('data', function (chunk) {
                    data += chunk;
                });
                
                res.on('end', function () {
                    if (status == 200) {
                        //console.log("downloadRawInstagramMedia status 200");
                        var answer = {};
                        answer = JSON.parse(data);
                        var media = answer.data.filter(function (el) {
                            return el.location != null;
                        });
                        if (typeof answer.pagination.next_url !== 'undefined') {
                            module.exports.downloadRawInstagramMedia(answer.pagination.next_url)
                            .then(function (othermedia) {
                                media = media.concat(othermedia);
                                resolve(media);
                            });
                        }
                        else {
                            resolve(media);
                        }
                    }
                    else {
                        console.log("Download instagram media failed");
                        reject();
                    }
                });
            });
            req.end();
        });
    },
    downloadInstagramPicture: function (instagrammedia) {
        var filehelper = require('./FileHelper.js');
        return filehelper.getFromUrl(instagrammedia.images.standard_resolution.url)
        .then(function (imagename) {
            var result = {};
            result.location_name = instagrammedia.location.name;
            result.location_latitude = instagrammedia.location.latitude;
            result.location_longitude = instagrammedia.location.longitude;
            result.imagename = imagename;
            if (instagrammedia.caption != null) {
                result.text = instagrammedia.caption.text;
            } else {
                result.text = "";
            }
            return result;
        })
        .catch(function (err) {
            console.log("Download instagram picture failed " + err);
            return null;
        });
    },
};
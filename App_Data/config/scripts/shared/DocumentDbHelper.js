var DocumentClient = require('documentdb').DocumentClient;
var Promise = require("es6-promise").Promise;
var appSettings = require('mobileservice-config').appSettings;

module.exports = {
	getDatabaseName : function() {
		return appSettings.DOCUMENT_DB_NAME;
	},
	getClient : function() {
		var host = appSettings.DOCUMENT_DB_HOST;
		var authKey = appSettings.DOCUMENT_DB_KEY;
		
		
		return new DocumentClient(host, { masterKey: authKey });
	},
	insertDocument : function(client, collection, documentDefinition) {
		
		return new Promise(function(resolve, reject) {
			client.createDocument(collection._self, documentDefinition, function(err, document) {
				if(err) 
				{
					console.log(err);
					reject("DocumentDb error");
				}
				else
				{
					resolve(document.content);
				}
			});
		});
		
	},
	readOrCreateDatabase : function (client, databaseId) {
	
		return new Promise(function(resolve, reject) {
			client.queryDatabases('SELECT * FROM root r WHERE r.id="' + databaseId + '"').toArray(function (err, results) {
			
				if (err) {
					
					
					for (var key in err)
					{
						console.log(err[key]);
					}
					
					reject("DocumentDB error");
				}
				else {
					if (results.length === 0) {
						// no error occured, but there were no results returned 
						// indicating no database exists matching the query            
						client.createDatabase({ id: databaseId }, function (err, createdDatabase) {
							resolve(createdDatabase);
						});
						
					} else {
						// we found a database
						resolve(results[0]);
					}
				}
			
			});
		});
		
    },
	readOrCreateCollection : function (client, database, collectionId) {
	
		return new Promise(function(resolve, reject) {
			client.queryCollections(database._self, 'SELECT * FROM root r WHERE r.id="' + collectionId + '"').toArray(function (err, results) {
				if (err) {
						
						for (var key in err)
						{
							console.log(err[key]);
						}
					reject("DocumentDB error");
				}
				else  {
					if (results.length === 0) {
						// no error occured, but there were no results returned 
						//indicating no collection exists in the provided database matching the query
						client.createCollection(database._self, { id: collectionId }, function (err, createdCollection) {
							resolve(createdCollection);
						});
					} else {
						// we found a collection
						resolve(results[0]);
					}
				}
			});
		});
	}

};
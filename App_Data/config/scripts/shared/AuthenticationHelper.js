var crypto = require('crypto');
var iterations = 1000;
var bytes = 32;


module.exports = {
    aud : "Custom",
    masterKey : "xhiGBLrJlMjsWZekLMMCZjfkFznlld88",
    
    validateToken: function (userId, token, masterKey) {

        function base64(input) {
            return new Buffer(input, 'utf8').toString('base64');
        }
        
        function urlFriendly(b64) {
            return b64.replace(/\+/g, '-').replace(/\//g, '_').replace(new RegExp("=", "g"), '');
        }
        function signature(input) {
            var key = crypto.createHash('sha256').update(masterKey + "JWTSig").digest('binary');
            var str = crypto.createHmac('sha256', key).update(input).digest('base64');
            return urlFriendly(str);
        }

        var valid = true;
        var tokenArray = token.split('.');
        
        var signature = signature(tokenArray[0] + "." + tokenArray[1]);
        
        if (signature != tokenArray[2]) {
            valid = false;
        } else {
            //var header = new Buffer(tokenArray[0], 'base64').toString('utf-8');
            var envelope = new Buffer(tokenArray[1], 'base64').toString('utf-8');
            
            var envelopeJson = JSON.parse(envelope);
            var expiry = new Date(parseFloat(envelopeJson.exp) * 1000);
            var now = new Date();
			
            if (expiry <= now) {
                valid = false;
            }
        }
        return valid;
    },
    getSalt : function () {
        return (new Buffer(crypto.randomBytes(bytes)).toString('base64'));
    },
    hash : function (text, salt, callback) {
        
        crypto.pbkdf2(text, salt, iterations, bytes, function (err, derivedKey) {
            if (err) { callback(err); }
            else {
                var h = new Buffer(derivedKey).toString('base64');
                callback(null, h);
            }
        });
    },
    slowEquals : function (a, b) {
        var diff = a.length ^ b.length;
        for (var i = 0; i < a.length && i < b.length; i++) {
            diff |= (a[i] ^ b[i]);
        }
        
        return a === b && diff === 0;
    },
    getJ2FromToken : function (token) {
		
    },
    zumoJwt : function (expiryDate, aud, userId, masterKey) {
        
		
        
        function base64(input) {
            return new Buffer(input, 'utf8').toString('base64');
        }
        
        function urlFriendly(b64) {
            return b64.replace(/\+/g, '-').replace(/\//g, '_').replace(new RegExp("=", "g"), '');
        }
        
        function signature(input) {
            var key = crypto.createHash('sha256').update(masterKey + "JWTSig").digest('binary');
            var str = crypto.createHmac('sha256', key).update(input).digest('base64');
            return urlFriendly(str);
        }
        
        var s1 = '{"alg":"HS256","typ":"JWT","kid":"0"}';
        var j2 = {
            "exp": expiryDate.valueOf() / 1000,
            "iss": "urn:microsoft:windows-azure:zumo",
            "ver": 2,
            "aud": aud,
            "uid": userId
        };
        
        var s2 = JSON.stringify(j2);
        var b1 = urlFriendly(base64(s1));
        var b2 = urlFriendly(base64(s2));
        var b3 = signature(b1 + "." + b2);
        return [b1, b2, b3].join(".");
		
    },
    randomString : function (length, chars) {
        if (!chars) {
            throw new Error('Argument \'chars\' is undefined');
        }
        
        var charsLength = chars.length;
        if (charsLength > 256) {
            throw new Error('Argument \'chars\' should not have more than 256 characters' 
				+ ', otherwise unpredictability will be broken');
        }
        
        var randomBytes = crypto.randomBytes(length)
        var result = new Array(length);
        
        var cursor = 0;
        for (var i = 0; i < length; i++) {
            cursor += randomBytes[i];
            result[i] = chars[cursor % charsLength]
        }        ;
        
        return result.join('');
    }
    ,randomAsciiString : function (length) {
        return this.randomString(length,
			'abcdefghjkmnpqrstuwxyzABCDEFGHJKLMNPQRSTUWXYZ123456789');
    },
};

﻿var Promise = require("es6-promise").Promise;
var fuzzy = require('fuzzy');

module.exports = {
	getLocationFromParent: function (request, response) {
	
		return new Promise(function(resolve, reject) {
						
			var refid = request.param("refid");
			
			var krumbTable = request.service.tables.getTable("dbkrumb");
			krumbTable.where({ id: refid }).read({
			
				success: function(results) {
					if (results.length > 0) {
						
						resolve(
						{ 
							id: results[0].location,
							longitude: results[0].longitude,
							latitude: results[0].latitude,
							name: results[0].locationname,
							beaconid: results[0].beaconid
						});
											
					}
					else {
						reject("refid not valid");
					}
				},
				error: function(err) {
					console.log("db error in getLocationFromParent: " + err);
					reject("db error");
				}
			});
			
		});
	
	},
    getLocation: function (request, response) {
        

        var promise = new Promise(function (resolve, reject) {
            
            var locationId = request.param("location_id");
            var locationName = request.param("location_name");
			var beaconid = request.param("beaconid", "");
            var longitude = parseFloat(request.param("location_long"));
            var latitude = parseFloat(request.param("location_lat"));

            if (typeof locationId != "undefined" && locationId.length > 0) {
                var location = {};
                location.id = locationId;
							
				var locationTable = request.service.tables.getTable("dblocation");
				locationTable.where({ id: locationId }).read({
				
					success: function(results) {
						if (results.length > 0) {
							
							results[0].id = locationId;
							resolve(results[0]);
						
						}
						else {
							reject("id not valid");
						}
					},
					error: function(err) {
						console.log("db error in getLocation: " + err);
						reject("db error");
					}
				});
				
            }
			else if (typeof beaconid != "undefined" && beaconid.length > 0) {
			
				var location = {};
                location.beaconid = beaconid;
							
				var locationTable = request.service.tables.getTable("dblocation");
				locationTable.where({ beaconid: beaconid }).read({
				
					success: function(results) {
						if (results.length > 0) {
							
							resolve(results[0]);
						
						}
						else {
							reject("beaconid not valid");
						}
					},
					error: function(err) {
						console.log("db error in getLocation: " + err);
						reject("db error");
					}
				});
			
			}
            else {
                if (typeof locationName == "undefined" || locationName.length < 3) {
                    reject(Error("invalid location name"));
                }
                else {
					
					var userid = request.user.userId;
					var ipinfo = request.headers['x-forwarded-for'];
					
					var closelocationcutoff = 50;
					var sql = "exec krumb.GetCloseLocations @longitude = ?, @latitude = ?, @distancemeters = ?";
					
					request.service.mssql.query(sql, [longitude, latitude, closelocationcutoff], {
						success: function (results){
											
							var options = {
								extract: function(el) { return el.name; }
							};
							var fuzzyresults = fuzzy.filter(locationName, results, options);
							var matches = fuzzyresults.map(function(el) { return el.original; });
							
							if (matches.length > 0) {
							
								resolve(matches[0]);
							
							}
							else {
								sql = "exec krumb.addLocation @name = ?, @longitude = ?, @latitude = ?, @userid = ?,@ip = ?";
								request.service.mssql.query(sql, [locationName, longitude, latitude, userid, ipinfo], {
									success: function (results){
										var location = {};
										location.id = results[0].id;
										location.longitude = longitude;
										location.latitude = latitude;
										location.name = locationName;
										resolve(location);
									},
									error: function (err) {
										console.log("error locationhelper: " + err);
										reject(err);
									}
								});
							}
						},
						error: function (err) {
							
							console.log("error locationhelper: " + err);
							reject(err);
							
						}
					});
                }
            }
        });
        
        return promise;
    }
};
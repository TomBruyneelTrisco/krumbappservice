var filehelper = require('./../shared/FileHelper');
var Promise = require("es6-promise").Promise;
var queuehelper = require('./../shared/QueueHelper');

exports.register = function (api) {
    //api.post('/create', function (request, response) {
    //    var userId = request.user.userId;
    //    var imagepath = "";
    //    var drawingpath = "";
    //    
    //    var userdbTable = request.service.tables.getTable("dbuser");
    //    userdbTable.where({ id: userId}).read({
    //        success: function(results) {
    //            filehelper.save(request, {field: "image", dir: "images" })
    //                .then(function(image){
    //                    if(image.length == 0) {
    //                        var imageblob = request.param("image", "");
    //                        if(imageblob.length > 0) {
    //                            return filehelper.moveAzureFile("uploads", imageblob, "images", imageblob);
    //                        }
    //                        else {
    //                            return "";
    //                        }
    //                    }
    //                    else {
    //                        return image;
    //                    }
    //                }).then(function (image) {
    //                    imagepath = image;
    //                    return filehelper.save(request, {field: "drawing", dir: "drawings"});   
    //                }).then(function(drawing){
    //                    if(drawing.length == 0){
    //                        var drawingblob = request.param("drawing", "");
    //                        if(drawingblob.length > 0) {
    //                            return filehelper.moveAzureFile("uploads", drawingblob, "drawings", drawingblob);
    //                        }
    //                        else{
    //                            return "";
    //                        }
    //                    }
    //                    else{
    //                        return drawing;
    //                    }
    //                }).then(function (drawing) {
    //                    drawingpath = drawing;
    //                    
    //                    var systemkrumb = {};
    //                    systemkrumb.text = request.param('text',"");
    //                    systemkrumb.image = imagepath;
    //                    systemkrumb.drawing = drawingpath;
    //                    systemkrumb.backgroundColor = request.param("backgroundColor", "");
    //                    systemkrumb.startDate = request.param("startDate", null);
    //                    systemkrumb.endDate = request.param("endDate", null);
    //                    systemkrumb.type = parseInt(request.param("type", 0));
    //                    systemkrumb.priority = parseInt(request.param("priority", null));
    //                    systemkrumb.targetGroup = parseInt(request.param("targetGroup", null));
    //                    systemkrumb.data = request.param("data", "");
    //                    
    //                    var systemkrumbTable = request.service.tables.getTable("dbsystemkrumb");
    //                    systemkrumbTable.insert(systemkrumb, {
    //                        success: function(results){
    //                            request.respond(200, {id: results.id});
    //                        },
    //                        error: function(err){
    //                            console.log("database error: " + err);
    //                            request.respond(500, {message: "database error"});
    //                        }
    //                    });
    //                })
    //                .catch(function(err){
    //                    console.log("Promise rejected " + err);
    //                    request.respond(500, "error while saving systemkrumb");
    //                });
    //        },
    //        error: function(err) {
    //            console.log("db error in createsystemkrumb: " + err);
    //            request.respond(500, "error while fetching user");
    //        }
    //    });
    //});
    
    //api.get('/list', function(request, response) {
    //    var userId = request.user.userId;
    //    
    //    var skip = parseInt(request.param("skip", "0"));
    //    var take = parseInt(request.param("take", "100"));
    //    
    //    if(take > 1000)
    //    {
    //        take = 1000;
    //    }
    //    var result = {};
    //    
    //    var systemkrumbTable = request.service.tables.getTable("dbsystemkrumb");
    //    result = systemkrumbTable.find().limit(take);
    //    
    //    request.respond(200, result);
    //});
    
    function shuffle(o){
        for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    }
    
    api.get('/random', function(request, response) {
        var systemkrumbTable = request.service.tables.getTable("dbsystemkrumb");
        
        systemkrumbTable.where(function(now){
            return this.__deleted == false
                && this.startdate < now
                && this.enddate > now;
        }, (new Date())).read({
            success: function(results) {
                if(results.length > 0)
                {
                var sumPriorities = 0;
                results.forEach(function(item) {
                    sumPriorities += item.priority;
                });
                
                if(sumPriorities > 0){
                    //Convert priority 0 to 1
                    sumPriorities += results.length;
                    results.forEach(function(item) {
                        item.priority = (item.priority + 1.00)/sumPriorities;
                    });
                    shuffle(results);
                    var random = Math.random();
                    //Reverse sorting, highest priority first
                    results.sort(function(a, b){
                        if(a.priority > b.priority) return -1;
                        if(a.priority < b.priority) return 1;
                        return 0;
                    });
                    results.every(function(item, index, array) {
                        if((item.priority >= random) || (index == (array.length-1)) ){
                            if (typeof item.createdAt != 'undefined') {
                                item.createdAt = item.createdAt.getTime();
                            }
                            item.startdate = item.startdate.getTime();
                            item.enddate = item.enddate.getTime();
                            request.respond(200, item);
                            //Jump out of loop
                            return false;
                        }
                        else{
                            return true;
                        }
                    });
                }
                else{
                    var random = Math.floor(Math.random() * results.length);
                    var item = results[random];
                    if (typeof item.createdAt != 'undefined') {
                        item.createdAt = item.createdAt.getTime();
                    }
                    item.startdate = item.startdate.getTime();
                    item.enddate = item.enddate.getTime();
                    request.respond(200, item);
                }
                }
                else
                {
                    request.respond(200, null);
                }
            },
            error: function(err) {
                console.log("db error: " + err);
                request.respond(500, {message: "DB error"});
            }
        });
    });
};
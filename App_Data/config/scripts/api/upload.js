var filehelper = require('./../shared/FileHelper');

exports.register = function(api) {
    
    api.post('/filepart', function(request, response) {
                
        if (request.files) {            
            if (request.files.upload) {
                
            }
        } else {
            request.respond("500", { code: 500, message: "a file upload with name upload is mandatory" } );
            return;
        }
        
        var chunk = parseInt(request.param("chunk", "0"));
        var chunks = parseInt(request.param("chunks", "0"));
        var blobname = "";
        
        if (chunk > 1) {
            blobname = request.param("blobname", "");
            
            if (blobname.length == 0) {
                request.respond("500", { code: 500, message: "blobname is mandatory when chunk is bigger than zero" } );
                return;
            }
        }
        
        filehelper.saveFilePart(request.files.upload, chunk, chunks, blobname, "uploads")
        .then(function(blobname) {
            request.respond(200, { success: true, blobname: blobname });
        }, function(err) {
            console.log(err);
            request.respond(500, { code: 500, success: false, message: "error during upload to storage" });
        });
        
    });
    
};
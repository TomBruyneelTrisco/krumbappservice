var Promise = require("es6-promise").Promise;
var socialmedia = require('./../shared/SocialMediaHelper');
var authhelper = require('./../shared/AuthenticationHelper');
var filehelper = require('./../shared/FileHelper');
var queuehelper = require('./../shared/QueueHelper');

function insertFollow(request, userid, idtofollow) {
		
	var promise = new Promise(function (resolve, reject) {
	
	
		var followTable = request.service.tables.getTable("dbfollow");
		followTable.insert({

			userid: userid,
			followid: idtofollow

		}, {
				success: function() {
				

					queuehelper.addServiceBusCommand("commands", "follow", { follower: userid, following: idtofollow })
						.then(function() {
							resolve();
						});
				},
				error: function(err) {
				

					request.service.mssql.query("UPDATE dbfollow SET __deleted=0 WHERE Userid=? AND Followid=?",
						[userid, idtofollow],
						{
							success: function() {
								resolve();
							},
							error: function(err) {
								console.log("database error: " + err);
								reject();
							}
						});
				}
			});
	});
	
	return promise;
}

exports.register = function(api) {
    
    api.get('/findbylocation', function(request, response) {
        var myUserId = request.user.userId;
        var username = request.param("username", "");
        var longitude = parseFloat(request.param("longitude", "0.0"));
        var latitude = parseFloat(request.param("latitude", "0.0"));
        var locationid = request.param("locationid", "");
        var ispartner = (request.param("ispartner", "false") == "false" ? 0 : 1);
        
        var sql = "exec krumb.getUserByLocation @username=?, @longitude=?, @latitude=?, @locationid=?, @ispartner=?, @myuserid=?";
				
		request.service.mssql.query(sql, [username, longitude, latitude, locationid, ispartner, myUserId], {
			success: function(cresults) {
                
                var restresults = [];
                
                cresults.forEach(function(cresult) {
                    
                    restresults.push({
                       username : cresult.username,
                       userid: cresult.userid,
                       ispartner: cresult.ispartner,
                       relation: cresult.relation
                    });
                    
                });
                
                request.respond(200, restresults);
                
            },
            error: function(err) {
				console.log("database error while fetching findbylocation: " + err);
				request.respond(500, { code: 500, message: "database error" });
			}
        });
    });
    
    api.post("/createSocialUser", function(request, response) {

        var socialMediaType = request.param("socialMediaType", "FB");
        var socialMediaId = request.param("socialMediaId", "");
        var socialMediaToken = request.param("socialMediaToken", "");
        var socialMediaVerifyUrl = request.param("socialMediaVerifyUrl", "https://api.twitter.com/1.1/account/verify_credentials.json");

        if (!(request.param('username', '').length > 3)) {

            request.respond(400, {
                code: 400,
                message: "Invalid username (at least 4 chars)"
            });
            return;

        }

        if (request.param('email', '').length == 0) {
            request.respond(400, { code: 400, message: "Email is required" });
        }

        var combinedid = socialMediaType + socialMediaId;

        socialmedia.checkUserLogin(socialMediaType, socialMediaId, socialMediaToken, socialMediaVerifyUrl)
            .then(function(loginsucceeded) {
				
				console.log("In resolve of checkUserLogin: " + loginsucceeded);
			
                if (loginsucceeded) {

                    var dbuserTable = request.service.tables.getTable('dbuser');
                    dbuserTable.where(function(currentUsername, currentEmail, currentCombinedId) {
                        return this.username == currentUsername
                            || this.email == currentEmail
							|| this.socialMediaId == currentCombinedId;
                    }, request.param('username'), request.param('email'), combinedid).read({
                            success: function(results) {
                                if (results.length > 0) {
                                    request.respond(400, {
                                        code: 400,
                                        message: "Username, social media user or email already exists"
                                    });
                                    return;
                                }
                                else {

                                    var dbuser = {};

                                    // Add your own validation - what fields do you require to 
                                    // add a unique salt to the item
                                    dbuser.salt = authhelper.getSalt();
                                    dbuser.password = request.param('password', 'social' + (new Date()).getTime());
                                    dbuser.username = request.param('username');
                                    dbuser.optin = (request.param('optin', "false").toLowerCase().toLowerCase() == "true" || request.param('optin', "false").toLowerCase().toLowerCase() == "1");
                                    dbuser.socialMediaId = combinedid;
                                    dbuser.mobilenumber = request.param("mobilenumber", "");
                                    dbuser.email = request.param('email', "");
                                    dbuser.twowayauth = false;
                                    // hash the password
                                    authhelper.hash(dbuser.password, dbuser.salt, function(err, h) {

                                        dbuser.password = h;
                                        dbuser.ip = request.headers['x-forwarded-for'];

                                        dbuserTable.insert(dbuser, {
                                            success: function(newUser) {
                                                var expiry = new Date();
                                                expiry.setUTCDate(expiry.getUTCDate() + 30);
                                                var userId = newUser.id;
												
												var respondobject = {
														username: newUser.username,
														email: newUser.email,
														id: newUser.id,
														token: authhelper.zumoJwt(expiry, authhelper.aud, userId, request.service.config.masterKey),
														expires: expiry.getTime()
													};
												
												if (socialMediaType == "AK") {
													insertFollow(request, userId, "1DA4A3A8-0970-4479-9D78-33F37EE09AA4").then(function() {
														request.respond(200, respondobject);
													}, function() {
														console.log("Error: AK user couldn't follow Stad Antwerpen user");
														
														request.respond(200, respondobject);
													});
												}
												else {
													request.respond(200, respondobject);
												}
                                            },
                                            error: function(err) {
												console.log(err);
                                                request.respond(500, {
                                                    code: 500,
                                                    message: "db error"
                                                });
                                            }
                                        })

                                    });

                                }
                            },
							error: function(err) {
								console.log(err);
								request.respond(500, {
                                                    code: 500,
                                                    message: "db error"
                                                });
							}
                        });
                } //else loginsucceeded
                else {

                    request.respond(500, {
                        code: 500,
                        "message": "invalid accesstoken"
                    });

                }
            });
    });

    api.post("/create", function(request, response) {


        if (!request.param('username', '').length > 3) {

            request.respond(400, {
                code: 400,
                message: "Invalid username (at least 3 chars)"
            });
            return;

        }

        if (request.param('password', '').length < 7) {

            request.respond(400, {
                code: 400,
                message: "Invalid password (least 7 chars required)"
            });
            return;

        }

        if (request.param('email', '').length == 0) {
            request.respond(400, { code: 400, message: "Email is required" });
            return;
        }

        var avatar = request.param("avatar", "");

        var dbuserTable = request.service.tables.getTable('dbuser');
        dbuserTable.where(function(currentUsername, currentEmail) {
            return this.username == currentUsername
                || this.email == currentEmail;
        }, request.param('username'), request.param('email')).read({
                success: function(results) {
                    if (results.length > 0) {
                        request.respond(400, {
                            code: 400,
                            message: "Username or email already exists"
                        });
                        return;
                    }
                    else {

                        var dbuser = {};

                        // Add your own validation - what fields do you require to 
                        // add a unique salt to the item
                        dbuser.salt = authhelper.getSalt();
                        dbuser.password = request.param('password');
                        dbuser.username = request.param('username');
                        dbuser.optin = (request.param('optin', "false").toLowerCase().toLowerCase() == "true" || request.param('optin', "false").toLowerCase().toLowerCase() == "1");
                        dbuser.email = request.param('email', "");
                        dbuser.mobilenumber = request.param("mobilenumber", "");
                        dbuser.twowayauth = false;
                        // hash the password
                        authhelper.hash(dbuser.password, dbuser.salt, function(err, h) {

                            dbuser.password = h;
                            dbuser.ip = request.headers['x-forwarded-for'];

                            dbuserTable.insert(dbuser, {
                                success: function(newUser) {

                                    var sendresponse = function() {
                                        var expiry = new Date();
                                        expiry.setUTCDate(expiry.getUTCDate() + 30);
                                        var userId = newUser.id;
                                        request.respond(200, {
                                            username: newUser.username,
                                            email: newUser.email,
                                            id: newUser.id,
                                            token: authhelper.zumoJwt(expiry, authhelper.aud, userId, request.service.config.masterKey),
                                            expires: expiry.getTime()
                                        });
                                    };

                                    if (avatar.length > 0) {
                                        queuehelper.addQueueCommand("uploads", { type: "avatar", userid: newUser.id, blobname: avatar }).then(function() {

                                            sendresponse();

                                        });
                                    }
                                    else
                                        sendresponse();
                                },
                                error: function(err) {
                                    request.respond(500, {
                                        code: 500,
                                        message: "db error: " + err
                                    });
                                }
                            })

					});

                    }
                }
            });
    });

    api.post("/update", function(request, response) {

        var dbuserTable = request.service.tables.getTable('dbuser');
        if (request.user.level != "authenticated") {
            request.respond(401, { message: "authentication required" });
        }

        if ((request.param('password', '').length != 0) && (request.param('password', '').length < 7)) {

            request.respond(400, {
                code: 400,
                message: "Invalid password (least 7 chars required)"
            });
            return;

        }

        var dbuser = {};

        var usernamechanged = false;

        dbuser.id = request.user.userId;

        var promisePassword = new Promise(function(resolve, reject) {
            if (request.param('password', '').length > 0) {
                // Add your own validation - what fields do you require to 
                // add a unique salt to the item
                dbuser.salt = authhelper.getSalt();
                dbuser.password = request.param('password');
                // hash the password
                authhelper.hash(dbuser.password, dbuser.salt, function(err, h) {
                    dbuser.password = h;
                    resolve();
                });
            }
            else {
                resolve();
            }
        });

        promisePassword.then(function() {

            //password hashed, check if email already exists
            return new Promise(function(resolve, reject) {
                if (request.param('email', '').length > 0) {

                    dbuserTable.where(function(currentEmail) {
                        return this.email == currentEmail;
                    }, request.param('email'))
                        .read(
                        {
                            success: function(results) {

                                if (results.length > 0) {
                                    request.respond(500, { code: 500, message: "Email already exists" });
                                    reject();
                                }
                                else {
                                    dbuser.email = request.param('email');
                                    resolve();
                                }

                            },
                            error: function(err) {
                                console.log("db error in update user: " + err);
                                request.respond(500, { code: 500, message: "db error" });
                                reject();
                            }
                        });


                }
                else {
                    resolve();
                }
            });

        }).then(function() {

                //check username valid
                return new Promise(function(resolve, reject) {
                    if (request.param('username', '').length > 3) {

                        dbuserTable.where(function(currentUsername) {
                            return this.username == currentUsername;
                        }, request.param('username'))
                            .read(
                            {
                                success: function(results) {

                                    if (results.length > 0) {
                                        request.respond(500, { code: 500, message: "Username already exists" });
                                        reject();
                                    }
                                    else {
                                        dbuser.username = request.param('username');
                                        usernamechanged = true;
                                        resolve();
                                    }

                                },
                                error: function(err) {
                                    console.log("db error in update user: " + err);
                                    request.respond(500, { code: 500, message: "db error" });
                                    reject();
                                }
                            });

                    }
                    else if (request.param('username', '').length > 0) {
                        request.respond(500, { code: 500, message: "Username too short, minimum 4 characters" });
                        reject();
                    }
                    else {
                        resolve();
                    }
                });

            }).then(function() {

                if (request.param('mobilenumber', '').length > 0) {
                    dbuser.mobilenumber = request.param("mobilenumber", "");
                }
                if (request.param('extrainfo', '').length > 0) {
                    dbuser.extrainfo = request.param("extrainfo", "");
                }
				if (request.param('website', '').length > 0) {
                    dbuser.website = request.param("website", "");
                }
                if (request.param("optin", '').length > 0) {
                    dbuser.optin = (request.param('optin', "false").toLowerCase().toLowerCase() == "true" || request.param('optin', "false").toLowerCase().toLowerCase() == "1");
                }

                dbuser.twowayauth = false;

                dbuser.ip = request.headers['x-forwarded-for'];

                dbuserTable.update(dbuser, {
                    success: function(result) {

                        var avatarpromise = new Promise(function(resolve, reject) {
                            var avatar = request.param("avatar", "");

                            if (avatar.length > 0) {
                                console.log("add avatar to queue!");

                                queuehelper.addQueueCommand("uploads", { type: "avatar", userid: dbuser.id, blobname: avatar }).then(function() {

                                    resolve();

                                });
                            }
                            else
                                resolve();

                        });

                        avatarpromise.then(function() {

                            return new Promise(function(resolve, reject) {

                                queuehelper.addServiceBusCommand("commands", "usernamechanged", { userid: dbuser.id })
                                    .then(function() {

                                        resolve();

                                    }, function() {
                                        //todo: admin followup needed
                                        resolve();
                                    });
                            });

                        }).then(function() {

                                request.respond(200, { success: true });

                            });

                    },
                    error: function(err) {
                        console.log("database error in user update: " + err);
                        request.respond(500, { code: 500, message: "database error" });
                    }
                });
            });
    });

    api.post("/follow", function(request, response) {

        if (request.user.level != "authenticated") {
            request.respond(401, { code: 401, message: "authentication required" });
            return;
        }

        var userid = request.user.userId;
        var idtofollow = request.param("targetid", "");

        if (idtofollow.length == 0) {
            request.respond(500, { code: 500, message: "targetid is mandatory" });
            return;
        }

        if (idtofollow.toLowerCase() == userid.toLowerCase()) {
            request.respond(500, { code: 500, message: "following yourself is a sure sign of a narcissistic personality disorder" });
            return;
        }

		insertFollow(request, userid, idtofollow).then(function() {
			request.respond(200, { success: true });
		}, function() {
			request.respond(500, { code: 500, success: false, message: "database error" });
		});

    });
	
	api.post("/unfollow", function(request, response) {

        if (request.user.level != "authenticated") {
            request.respond(401, { code: 401, message: "authentication required" });
            return;
        }

        var userid = request.user.userId;
        var idtofollow = request.param("targetid", "");

        if (idtofollow.length == 0) {
            request.respond(500, { code: 500, message: "targetid is mandatory" });
            return;
        }

        request.service.mssql.query("UPDATE dbfollow SET __deleted=1 WHERE Userid=? AND Followid=?",
            [userid, idtofollow],
            {
                success: function() {
                    request.respond(200, { success: true });
                },
                error: function(err) {
                    console.log("database error: " + err);
                    request.respond(500, { code: 500, success: false, message: "database error" });
                }
            });

    });

    api.post("/report", function(request, response) {

        if (request.user.level != "authenticated") {
            request.respond(401, { code: 401, message: "authentication required" });
            return;
        }

        var userid = request.user.userId;
        var idtoreport = request.param("targetid", "");
        var reason = request.param("reason", "");

        if (idtoreport.length == 0) {
            request.respond(500, { code: 500, message: "targetid is mandatory" });
            return;
        }

        var dbreportuserTable = request.service.tables.getTable("dbreportuser");
        dbreportuserTable.insert({
            reporterid: userid,
            reportedid: idtoreport,
            reason: reason
        },
            {
                success: function() {
                    request.respond(200, { success: true });
                },
                error: function(err) {
                    console.log("database error: " + err);
                    request.respond(500, { code: 500, success: false, message: "database error" });
                }
            });

    });

    api.get("/find", function(request, response) {

        var username = request.param("username", "");

        if (username.length < 2) {
            request.respond(500, { code: 500, message: "username is mandatory and must have a length of at least 2 characters" });
            return;
        }
        
        username += "%";

        request.service.mssql.query("SELECT id, username FROM dbuser WHERE username LIKE ?",
            [username], {
                success: function(results) {

                    request.respond(200, results);

                },
                error: function(err) {
                    console.log("database error: " + err);
                    request.respond(500, { code: 500, message: "database error" });
                }
            });
    });

    api.get("/followers", function(request, response) {
		
		var myUserId = request.user.userId;

        var userid = request.param("userid", "");
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));

        if (userid.length == 0) {
            request.respond(500, { message: "userid is mandatory" });
            return;
        }

        var sql = "exec krumb.getUserFollowers @userid=?, @myuserid=?, @skip=?, @take=?";
					
		request.service.mssql.query(sql, [userid, myUserId, skip, take], {
			success: function(results) {
                
						
				var result2 = [];
                
                for (var i = 0; i < results.length; i++) {
                    
                    results[i].countkrumb = 10;
                    
                    result2.push(results[i]);
                }
			
                request.respond(200, result2);
			},
			error: function(err) {
				console.log("db error: " + err);
				request.respond(500, { code: 500, message: "db error" });
			}
		});
		
	});
	
	api.get("/following", function(request, response) {
		
		var myUserId = request.user.userId;

        var userid = request.param("userid", "");
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));

        if (userid.length == 0) {
            request.respond(500, { message: "userid is mandatory" });
            return;
        }

        var sql = "exec krumb.getUserFollowing @userid=?, @myuserid=?, @skip=?, @take=?";
					
		request.service.mssql.query(sql, [userid, myUserId, skip, take], {
			success: function(results) {
						
				var result2 = [];
                
                for (var i = 0; i < results.length; i++) {
                    
                    results[i].countkrumb = 10;
                    
                    result2.push(results[i]);
                }
                
                request.respond(200, result2);
			
			},
			error: function(err) {
				console.log("db error: " + err);
				request.respond(500, { code: 500, message: "db error" });
			}
		});
		
	});
	
	api.get("/view", function(request, response) {

        if (request.user.level != "authenticated") {
            request.respond(401, { code: 401, message: "authentication required" });
            return;
        }

        var userId = request.param("id", "");
        var myself = false;

        if (userId == "") {

            myself = true;
            userId = request.user.userId;

        }

        var dbuserTable = request.service.tables.getTable('dbuser');

        function getFollowers(userid) {
            var promisefollowers = new Promise(function(resolve, reject) {

                request.service.mssql.query("exec krumb.getFollowList @userid=?, @fulllist=?", 
                    [userId, (myself ? 1 : 0)], {
                    success: function(results) {

                        resolve(results);

                    },
                    error: function(err) {
                        console.log("database error: " + err);

                        reject(err);
                    }
                });
            });

            return promisefollowers;
        };
        
        request.service.mssql.query("exec krumb.getUser @userid=?, @myuserid=?", 
                [userId, request.user.userId], {
                success: function(results) {

                    if (results.length > 0) {

                        var fetchuser = results[0];
                        
                        getFollowers(userId).then(function(resultsfollowers) {
                            
                            if (myself) {
                                fetchuser.followers = resultsfollowers.splice(1, resultsfollowers.length - 1);
                                //stupid hack as node.js mssql doesn't support multiple tables
                                fetchuser.followingcount = resultsfollowers[0].Direction;
                                fetchuser.followercount = resultsfollowers[0].id;
                            } else {
                                fetchuser.followingcount = resultsfollowers[0].countfollowing;
                                fetchuser.followercount = resultsfollowers[0].countfollower;
                                
                                fetchuser.createdAt = null;
                                fetchuser.optin = null;
                            }

                            request.respond(200, fetchuser);

                        }, function(err) {

                            request.respond(500, { code: 500, message: "error fetching followers" });

                        });

                    }
                    else {
                        request.respond(404, { code: 404, message: "not found" });
                    }
                },
                error: function(err) {
                    console.log("database error: " + err);
                    request.respond(500, { code: 500, message: "database error" });
                }
            });


    });
};

var locationhelper = require('./../shared/LocationHelper');
var filehelper = require('./../shared/FileHelper');
var Promise = require("es6-promise").Promise;
var queuehelper = require('./../shared/QueueHelper');

var distanceEnum = {};
distanceEnum.far = 5000;
distanceEnum.close = 1000;
distanceEnum.nearby = 200;
distanceEnum.at = 50;

exports.register = function (api) {
    api.post('/like', function (request, response) {
        
        var userid = request.user.userId;
        var krumbid = request.param("krumbid", "");
        if (krumbid.length == 0) {
            
            request.respond("500", { code: 500, message: "the krumbid parameter is mandatory" });
            return;

        }
        
        queuehelper.addServiceBusCommand("commands", "likeadded", { krumbid: krumbid, userid: userid })
            .then(function () {
            
            request.respond(200, { success: true });
                
        }, function (err) {
            
            console.log("the created like could not be queued: " + err);
            request.respond(500, { code: 500, message: "Error while queueing like" });

        });
    });
    
    api.post('/unlike', function (request, response) {
        
        var userid = request.user.userId;
        var krumbid = request.param("krumbid", "");
        if (krumbid.length == 0) {
            
            request.respond("500", { message: "the krumbid parameter is mandatory" });
            return;
            
        }
        
        queuehelper.addServiceBusCommand("commands", "likeremoved", { krumbid: krumbid, userid: userid })
            .then(function () {
            
            request.respond(200, { success: true });
                
        }, function (err) {
            
            console.log("the removed like could not be queued: " + err);
            request.respond(500, { code: 500, message: "Error while queueing like removal" });

        });
    });
    
    api.post('/comment', function (request, response) {
        var userId = request.user.userId;
        var refid = request.param("refid", "");
        
        if (refid.length == 0) {
            request.respond("500", "the refid parameter is mandatory");
            return;
        }
        
        var imagepath = "";
        var drawingpath = "";
        
        var userdbTable = request.service.tables.getTable("dbuser");
        userdbTable.where({ id: userId }).read({
            success: function (results) {
                filehelper.save(request, { field: "image", dir: "images" })
                    .then(function (image) {
                    
                    if (image.length == 0) {
                        var imageblob = request.param("image", "");
                        
                        if (imageblob.length > 0) {
                            return filehelper.moveAzureFile("uploads", imageblob, "images", imageblob);
                        }
                        else
                            return "";
                    }
                    else {
                        return image;
                    }

                }).then(function (image) {
                    
                    imagepath = image;
                    
                    return filehelper.save(request, { field: "drawing", dir: "drawings" });
                }).then(function (drawing) {
                    
                    if (drawing.length == 0) {
                        var drawingblob = request.param("drawing", "");
                        
                        if (drawingblob.length > 0) {
                            return filehelper.moveAzureFile("uploads", drawingblob, "drawings", drawingblob);
                        }
                        else
                            return "";
                    }
                    else {
                        return drawing;
                    }

                }).then(function (drawing) {
                    
                    drawingpath = drawing;
                    
                    return locationhelper.getLocationFromParent(request);
                }).then(function (location) {
                    
                    var validity = request.param("validity", 0);
                    
                    var krumb = {};
                    krumb.text = request.param('text', "");
                    krumb.userid = userId;
                    krumb.username = results[0].username;
                    krumb.heading = parseInt(request.param('heading', 0));
                    krumb.location = location.id;
                    krumb.image = imagepath;
                    krumb.drawing = drawingpath;
                    krumb.backgroundColor = request.param("backgroundColor", "");
                    krumb.ip = request.headers['x-forwarded-for'];
                    krumb.longitude = location.longitude;
                    krumb.latitude = location.latitude;
                    krumb.locationname = location.name;
                    krumb.beaconid = location.beaconid;
                    krumb.refid = refid;
                    krumb.validuntil = null;
                    
                    var krumbTable = request.service.tables.getTable("dbkrumb");
                    krumbTable.insert(krumb, {
                        success: function (results) {
                            
                            krumb.id = results.id;
                            krumb.numbercomments = 0;
                            krumb.mylike = 0;
                            krumb.following = 0;
                            krumb.distance = 0;
                            krumb.comments = [];
                            krumb.location = location;
                            krumb.locationid = location.id;
                            
                            queuehelper.addServiceBusCommand("commands", "commentadded", { krumbid: refid, userid: userId, commentid: results.id })
                                    .then(function () {
                                
                                request.respond(200, krumb);
                            }, function (err) {
                                
                                //todo: this should be resolved somehow
                                console.log("the created krumb could not be queued for weight calc: " + err);
                                request.respond(200, krumb);

                            });

                        },
                        error: function (err) {
                            console.log("database error: " + err);
                            request.respond(500, { code: 500, message: "database error" });
                        }
                    });
                })
                    .catch(function (err) {
                    console.log("Promise rejected " + err);
                    request.respond(500, { code: 500, message: "error with location while saving krumb" });
                });
            },
            error: function (err) {
                request.respond(500, { code: 500, message: "error while fetching user" });
            }
        });
    });
    
    api.post("/report", function (request, response) {
        
        var userid = request.user.userId;
        var idtoreport = request.param("targetid", "");
        var reason = request.param("reason", "");
        
        if (idtoreport.length == 0) {
            request.respond(500, { code: 500, message: "targetid is mandatory" });
            return;
        }
        
        var dbreportkrumbTable = request.service.tables.getTable("dbreportkrumb");
        dbreportkrumbTable.insert({
            reporterid: userid,
            reportedid: idtoreport,
            reason: reason
        },
            {
            success: function () {
                request.respond(200, { success: true });
            },
            error: function (err) {
                console.log("database error: " + err);
                request.respond(500, { success: false, message: "database error" });
            }
        });

    });
    
    api.post('/delete', function (request, response) {
        var userid = request.user.userId;
        var krumbid = request.param("krumbid", "");
        
        console.log("requesting delete for krumbid: " + krumbid);
        
        if (krumbid.length == 0) {
            request.respond("500", "the krumbid parameter is mandatory");
            return;
        }
        
        queuehelper.addServiceBusCommand("commands", "krumbdeleted", { userid: userid, krumbid: krumbid })
        .then(function () {
            request.respond(200, { success: true });
        }, function (err) {
            console.log("the created krumb could not be queued for krumbadded: " + err);
            request.respond(500, { code: 500, message: "The krumb could not be queued for deletion" });

        });
    });
    
    api.post('/create', function (request, response) {
        var userId = request.user.userId;
        
        var imagepath = "";
        var drawingpath = "";
        
        var visibility = parseInt(request.param('visibility', "0"));
        var idstr = "";
        var isbranchshare = (request.param('isbranchshare', "false").toLowerCase() === "true");
        
        if (visibility == 1 && !isbranchshare) {
            idstr = request.param("to", "");
            
            if (idstr.length == 0) {
                request.respond(500, { code: 500, message: "to is mandatory" });
                return;
            }
        }
        
        var userdbTable = request.service.tables.getTable("dbuser");
        userdbTable.where({ id: userId }).read({
            success: function (results) {
                filehelper.save(request, { field: "image", dir: "images" })
                    .then(function (image) {
                    
                    if (image.length == 0) {
                        var imageblob = request.param("image", "");
                        
                        if (imageblob.length > 0) {
                            return filehelper.moveAzureFile("uploads", imageblob, "images", imageblob);
                        }
                        else
                            return "";
                    }
                    else {
                        return image;
                    }

                }).then(function (image) {
                    
                    imagepath = image;
                    
                    return filehelper.save(request, { field: "drawing", dir: "drawings" });
                }).then(function (drawing) {
                    
                    if (drawing.length == 0) {
                        var drawingblob = request.param("drawing", "");
                        
                        if (drawingblob.length > 0) {
                            return filehelper.moveAzureFile("uploads", drawingblob, "drawings", drawingblob);
                        }
                        else
                            return "";
                    }
                    else {
                        return drawing;
                    }

                }).then(function (drawing) {
                    
                    drawingpath = drawing;
                    
                    return locationhelper.getLocation(request);
                }).then(function (location) {
                    
                    var validity = request.param("validity", 0);
                    
                    var krumb = {};
                    krumb.text = request.param('text', "");
                    krumb.userid = userId;
                    krumb.username = results[0].username;
                    krumb.heading = parseInt(request.param('heading', 0));
                    krumb.location = location.id;
                    krumb.image = imagepath;
                    krumb.drawing = drawingpath;
                    krumb.backgroundColor = request.param("backgroundColor", "");
                    krumb.ip = request.headers['x-forwarded-for'];
                    krumb.longitude = location.longitude;
                    krumb.latitude = location.latitude;
                    krumb.locationname = location.name;
                    krumb.visibility = visibility;
                    krumb.beaconid = request.param("beaconid", null);
                    
                    var asyncTasks = [];
                    var chain;
                    
                    if (validity > 0)
                        krumb.validuntil = new Date((new Date()).getTime() + Number(validity));
                    else
                        krumb.validuntil = null;
                    
                    var krumbTable = request.service.tables.getTable("dbkrumb");
                    krumbTable.insert(krumb, {
                        success: function (results) {
                            
                            if (visibility == 1 && !isbranchshare) {
                                
                                var ids = idstr.split(",");
                                
                                for (var i = 0; i < ids.length; i++) {
                                    asyncTasks.push(new Promise(function (resolve, reject) {
                                        
                                        var privateTable = request.service.tables.getTable("dbprivatekrumbs");
                                        
                                        var privkrumb = {};
                                        privkrumb.foruser = ids[i];
                                        privkrumb.krumbid = results.id;
                                        
                                        privateTable.insert(privkrumb, {
                                            success: function (privresults) {
                                                resolve();
                                            },
                                            error: function (err) {
                                                console.log("db error: " + err);
                                                resolve();
                                            }

                                        });


                                    }));
                                }
                                
                                chain = asyncTasks.reduce(function (previous, item) {
                                    
                                    return previous.then(function () {
                                        return item;
                                    });
                                }, Promise.resolve());
                            }
                            else {
                                chain = Promise.resolve();
                            }
                            
                            chain.then(function () {
                                queuehelper.addServiceBusCommand("commands", "krumbadded", { userid: userId, krumbid: results.id })
                                        .then(function () {
                                    
                                    krumb.id = results.id;
                                    krumb.numbercomments = 0;
                                    krumb.mylike = 0;
                                    krumb.following = 0;
                                    krumb.beaconid = request.param("beaconid", null);
                                    krumb.distance = 0;
                                    
                                    krumb.location = location;
                                    krumb.comments = [];
                                    
                                    request.respond(200, krumb);
                                }, function (err) {
                                    
                                    //todo: this should be resolved somehow
                                    console.log("the created krumb could not be queued for krumbadded: " + err);
                                    request.respond(200, { id: results.id, locationid: location.id });

                                });
                            });
                        },
                        error: function (err) {
                            console.log("database error: " + err);
                            request.respond(500, { message: "database error" });
                        }
                    });
                })
                    .catch(function (err) {
                    console.log("Promise rejected " + err);
                    request.respond(500, "error while saving krumb");
                });
            },
            error: function (err) {
                console.log("db error in createkrumb: " + err);
                request.respond(500, "error while fetching user");
            }
        });
    });
    
    api.get('/likes', function (request, response) {
        var userId = request.user.userId;
        
        var krumbid = request.param("krumbid", "");
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        if (krumbid.length == 0) {
            request.respond(500, { message: "krumbid is mandatory" });
            return;
        }
        
        var sql = "exec krumb.getKrumbLikesCount @userid=?, @krumbid=?";
        
        request.service.mssql.query(sql, [userId, krumbid], {
            success: function (results) {
                if (results.length > 0) {
                    
                    var sql = "exec krumb.getKrumbLikes @krumbid=?, @myuserid=?, @skip=?, @take=?";
                    
                    request.service.mssql.query(sql, [krumbid, userId, skip, take], {
                        success: function (resultlikes) {
                            
                            results[0].likes = resultlikes;
                            
                            request.respond(200, results[0]);
						
                        },
                        error: function (err) {
                            console.log("db error: " + err);
                            request.respond(500, { code: 500, message: "db error" });
                        }
                    });
                }
            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, { code: 500, message: "db error" });
            }
        });
    });
    
    api.get('/listfromuser', function (request, response) {
        var userId = parseInt(request.param("userid", ""));
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        if (userId.length == 0) {
            request.respond(500, { message: "userid is mandatory" });
            return;
        }
        
        var sql = "exec krumb.getMyKrumbs @userid=?, @skip=?, @take=?;";
        
        request.service.mssql.query(sql, [userId, skip, take], {
            success: function (results) {
                var krumbs = [];
                var krumb = {};
                results.forEach(function (result) {
                    krumb = result;
                    krumb.createdAt = result.createdAt.getTime();
                    if (typeof result.updatedAt != 'undefined') {
                        krumb.updatedAt = result.updatedAt.getTime();
                    }
                    krumbs.push(krumb);
                });
                request.respond(200, krumbs);
            },
            error: function (err) {
                request.respond(500, { code: 500, message: err });
            }
        });
    });
    
    api.get('/listmycomments', function (request, response) {
        var userId = request.user.userId;
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        var sql = "SELECT COUNT(*) AS total FROM krumb.dbkrumb WHERE __deleted=0 AND refid IS NOT NULL AND userid=?";
        
        request.service.mssql.query(sql, [userId], {
            success: function (results) {
                
                var count = results[0].total;
                
                var sql = "exec krumb.getMyKrumbComments @userid=?, @skip=?, @take=?;";
                
                request.service.mssql.query(sql, [userId, skip, take], {
                    success: function (results) {
                        var krumbs = {
                            krumbs: [],
                            total: count
                        };
                        var krumb = {};
                        results.forEach(function (result) {
                            krumb = result;
                            krumb.createdAt = result.createdAt.getTime();
                            if (typeof result.updatedAt != 'undefined') {
                                krumb.updatedAt = result.updatedAt.getTime();
                            }
                            krumbs.krumbs.push(krumb);
                        });
                        request.respond(200, krumbs);
                    },
                    error: function (err) {
                        console.log("listmine error reading proc: " + err);
                        request.respond(500, { code: 500, message: err });
                    }
                });
            },
            error: function (err) {
                console.log("listmine error reading count: " + err);
                request.respond(500, { code: 500, message: err });
            }
        });
    });
    
    api.get('/listmine', function (request, response) {
        var userId = request.user.userId;
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        var sql = "SELECT COUNT(*) AS total FROM krumb.dbkrumb WHERE __deleted=0 AND refid IS NULL AND userid=?";
        
        request.service.mssql.query(sql, [userId], {
            success: function (results) {
                
                var count = results[0].total;
                
                var sql = "exec krumb.getMyKrumbs @userid=?, @skip=?, @take=?;";
                
                request.service.mssql.query(sql, [userId, skip, take], {
                    success: function (results) {
                        var krumbs = {
                            krumbs: [],
                            total: count
                        };
                        var krumb = {};
                        results.forEach(function (result) {
                            krumb = result;
                            krumb.createdAt = result.createdAt.getTime();
                            if (typeof result.updatedAt != 'undefined') {
                                krumb.updatedAt = result.updatedAt.getTime();
                            }
                            krumbs.krumbs.push(krumb);
                        });
                        request.respond(200, krumbs);
                    },
                    error: function (err) {
                        console.log("listmine error reading proc: " + err);
                        request.respond(500, { code: 500, message: err });
                    }
                });
            },
            error: function (err) {
                console.log("listmine error reading count: " + err);
                request.respond(500, { code: 500, message: err });
            }
        });
    });
    
    api.get('/comments', function (request, response) {
        
        var krumbid = request.param("krumbid", "");
        var userId = request.user.userId;
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        if (krumbid.length == 0) {
            request.respond(500, { code: 500, message: "krumbid is mandatory" });
            return;
        }
        
        if (take <= 0) {
            request.respond(500, { code: 500, message: "Take should be bigger than zero" });
            return;
        }
        
        if (skip < 0) {
            request.respond(500, { code: 500, message: "Skip should be bigger or equal to zero" });
            return;
        }
        
        var sql = "exec krumb.getKrumbComments @userid=?, @krumbid=?, @skip=?, @take=?";
        
        request.service.mssql.query(sql, [userId, krumbid, skip, take], {
            success: function (cresults) {
                
                var results = [];
                
                if (cresults.length > 0) {
                    
                    cresults.forEach(function (cresult) {
                        var comment = {};
                        
                        comment.id = cresult.id;
                        comment.userid = cresult.userid;
                        comment.username = cresult.username;
                        comment.text = cresult.text;
                        comment.image = cresult.image;
                        comment.drawing = cresult.drawing;
                        comment.location = {};
                        comment.location.longitude = cresult.longitude;
                        comment.location.latitude = cresult.latitude;
                        comment.location.name = cresult.name;
                        comment.createdAt = cresult.createdAt.getTime();
                        
                        results.push(comment);
                    });
                }
                
                request.respond(200, results);
            },
            error: function (err) {
                console.log("database error while fetching comments: " + err);
                console.log(userId);
                console.log(krumbid);
                console.log(skip);
                console.log(take);
                
                request.respond(500, { code: 500, message: "Db error" });
            }
        });

    });
    
    api.post('/setprivatekrumbread', function (request, response) {
        var userId = request.user.userId;
        var krumbid = request.param("krumbid", "");
        
        if (krumbid.length == 0) {
            request.respond(500, { message: "krumbid is mandatory" });
            return;
        }
        
        var sql = "UPDATE krumb.dbprivatekrumbs SET isread=1 WHERE foruser=? AND krumbid=?";
        
        request.service.mssql.query(sql, [userId, krumbid], {
            success: function (results) {
                
                
                request.respond(200, { success: true });

            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, { code: 500, message: "db error while updating read status" });
            }
        });
    });
    
    api.get('/inboxcountunread', function (request, response) {
        var userId = request.user.userId;
        
        var out = [];
        
        //filters
        var sql = "";
        var params = [];
        var filter = request.param("filter", "none");
        
        if (filter == "here" || filter == "elsewhere") {
            var longitude = parseFloat(request.param("long", 0.0));
            var latitude = parseFloat(request.param("lat", 0.0));
            
            sql = "exec krumb.getPrivateKrumbsUnread @userid=?, @filter=?, @longitude=?, @latitude=?";
            params = [userId, filter, longitude, latitude];
			
        }
        else {
            sql = "exec krumb.getPrivateKrumbsUnread @userid=?";
            params = [userId];
        }
        
        request.service.mssql.query(sql, params, {
            success: function (results) {
                
                request.respond(200, { 'count': results[0].UnreadCount });

            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, { code: 500, message: "db error while fetching private krumbs- count" });
            }
        });

    });
    
    api.get('/inbox', function (request, response) {
        var userId = request.user.userId;
        
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "100"));
        
        var out = [];
        
        //filters
        var sql = "";
        var params = [];
        var filter = request.param("filter", "none");
        
        if (filter == "here" || filter == "elsewhere") {
            var longitude = parseFloat(request.param("long", 0.0));
            var latitude = parseFloat(request.param("lat", 0.0));
            
            sql = "exec krumb.getPrivateKrumbs @userid=?, @skip=?, @take=?, @filter=?, @longitude=?, @latitude=?";
            params = [userId, skip, take, filter, longitude, latitude];
			
        }
        else {
            sql = "exec krumb.getPrivateKrumbs @userid=?, @skip=?, @take=?";
            params = [userId, skip, take];
        }
        
        request.service.mssql.query(sql, params, {
            success: function (results) {
                
                results.forEach(function (result) {
                    var krumb = {};
                    krumb.id = result.id;
                    krumb.userid = result.userid;
                    krumb.username = result.username;
                    krumb.text = result.text;
                    krumb.image = result.image;
                    krumb.drawing = result.drawing;
                    krumb.numberlikes = result.numberlikes;
                    krumb.numbercomments = result.numbercomments;
                    krumb.isread = result.isread;
                    krumb.createdAt = result.createdAt.getTime();
                    
                    if (result.validuntil)
                        krumb.validuntil = result.validuntil.getTime();
                    else
                        krumb.validuntil = 0;
                    
                    krumb.location = {};
                    krumb.location.longitude = result.longitude;
                    krumb.location.latitude = result.latitude;
                    krumb.location.name = result.name;
                    out.push(krumb);
                });
                
                request.respond(200, out);

            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, { code: 500, message: "db error while fetching following krumbs" });
            }
        });

    });
    
    api.get('/listbybeacon', function (request, response) {
        var userId = request.user.userId;
        
        var beaconid = request.param("beaconid", "");
        
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "100"));
        var filterprivate = (request.param("filterprivate", "false") == "true");
        
        
        if (take > 1000) {
            take = 1000;
        }
        
        var result = {};
        var asyncTasks = [];
        
        var commentspromise = function (forkrumb) {
            return new Promise(function (resolve, reject) {
                
                if (forkrumb.numbercomments > 0) {
                    var sql = "exec krumb.getKrumbComments @userid=?, @krumbid=?, @skip=?, @take=?";
                    
                    
                    request.service.mssql.query(sql, [userId, forkrumb.id, 0, 20], {
                        success: function (cresults) {
                            
                            if (cresults.length > 0) {
                                
                                cresults.forEach(function (cresult) {
                                    var comment = {};
                                    
                                    comment.id = cresult.id;
                                    comment.userid = cresult.userid;
                                    comment.username = cresult.username;
                                    comment.text = cresult.text;
                                    comment.image = cresult.image;
                                    comment.drawing = cresult.drawing;
                                    comment.location = {};
                                    comment.location.longitude = cresult.longitude;
                                    comment.location.latitude = cresult.latitude;
                                    comment.location.name = cresult.name;
                                    comment.createdAt = cresult.createdAt.getTime();
                                    
                                    forkrumb.comments.push(comment);
                                });
                            }
                            
                            resolve();
                        },
                        error: function (err) {
                            console.log("database error while fetching comments: " + err);
                            resolve();
                        }
                    });
                }
                else {
                    resolve();
                }

            });
        }
        
        var sql = "";
        
        if (filterprivate)
            sql = "exec krumb.getByBeaconIdPrivate @beaconid=?, @userid=?, @skip=?, @take=?";
        else
            sql = "exec krumb.getByBeaconId @beaconid=?, @userid=?, @skip=?, @take=?";
        
        request.service.mssql.query(sql, [beaconid, userId, skip, take], {
            success: function (results) {
                if (results.length > 0) {
                    
                    var krumbs = [];
                    var total = results.length;
                    var count = 0;
                    
                    results.forEach(function (result) {
                        var krumb = {};
                        krumb.weight = result.weight / 16000;
                        krumb.id = result.id;
                        krumb.userid = result.userid;
                        krumb.username = result.username;
                        krumb.text = result.text;
                        krumb.image = result.image;
                        krumb.drawing = result.drawing;
                        krumb.numberlikes = result.numberlikes;
                        krumb.numbercomments = result.numbercomments;
                        krumb.mylike = result.mylike;
                        krumb.createdAt = result.createdAt.getTime();
                        
                        if (result.validuntil)
                            krumb.validuntil = result.validuntil.getTime();
                        else
                            krumb.validuntil = 0;
                        
                        krumb.location = {};
                        krumb.location.longitude = result.longitude;
                        krumb.location.latitude = result.latitude;
                        krumb.location.name = result.name;
                        krumbs.push(krumb);
                        krumb.comments = [];
                        
                        asyncTasks.push(commentspromise(krumb));

                    });
                    
                    var chain = asyncTasks.reduce(function (previous, item) {
                        
                        return previous.then(function () {
                            return item;
                        });

                    }, Promise.resolve());
                    
                    chain.then(function () {
                        result.krumbs = krumbs;
                        request.respond(200, result);
                    });
                }
                else {
                    request.respond(200, []);
                }
            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, "db error");
            }
        });

    });
    
    api.get('/listv3', function (request, response) {
        var userId = request.user.userId;
        
        var longitude = parseFloat(request.param("longitude", "0.0"));
        var latitude = parseFloat(request.param("latitude", "0.0"));
        var beaconids = request.param("beaconids", "");
        
        var beaconweights = {};
        if (beaconids.length > 0) {
            
            var parts = beaconids.split(",");
            
            for (var i = 0; i < parts.length; i++) {
                
                beaconweights["id" + parts[i]] = parts.length - i;
                
            }
            
        }
        
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "100"));
        
        var filterprivate = (request.param("filterprivate", "false") == "true");
        
        if (take > 1000) {
            take = 1000;
        }
        
        var result = {};
        result.krumbs = [];
        var asyncTasks = [];
        
        var calculateWeight = function (krumb, radius) {
            
            var baseweightdistance = 10000;
            var weight = krumb.weight;
            
            //calculate distance
            weight += baseweightdistance * (radius - krumb.radius) / radius;
            
            if (krumb.userid == userId)
                weight += 8000;
            if (krumb.following == 1)
                weight += 4000;
            
            if (krumb.beaconid) {
                if (beaconweights.hasOwnProperty("id" + krumb.beaconid)) {
                    weight += 30000 * beaconweights["id" + krumb.beaconid];
                }
            }
            
            return Math.ceil(weight);
			
        }
        
        var commentspromise = function (forkrumb) {
            return new Promise(function (resolve, reject) {
                
                var sql = "exec krumb.getKrumbComments @userid=?, @krumbid=?, @skip=?, @take=?";
                
                request.service.mssql.query(sql, [userId, forkrumb.id, 0, 20], {
                    success: function (cresults) {
                        
                        if (cresults.length > 0) {
                            
                            cresults.forEach(function (cresult) {
                                var comment = {};
                                
                                comment.id = cresult.id;
                                comment.userid = cresult.userid;
                                comment.username = cresult.username;
                                comment.text = cresult.text;
                                comment.image = cresult.image;
                                comment.drawing = cresult.drawing;
                                comment.numberlikes = cresult.numberlikes;
                                comment.numbercomments = cresult.numbercomments;
                                comment.mylike = cresult.mylike;
                                comment.following = cresult.following;
                                comment.beaconid = cresult.beaconid;
                                comment.location = {};
                                comment.location.longitude = cresult.longitude;
                                comment.location.latitude = cresult.latitude;
                                comment.location.name = cresult.name;
                                comment.location.id = cresult.location;
                                comment.location.beaconid = cresult.beaconid;
                                comment.createdAt = cresult.createdAt.getTime();
                                
                                forkrumb.comments.push(comment);
                            });
                        }
                        
                        resolve();
                    },
                    error: function (err) {
                        console.log("database error while fetching comments listv2: " + err);
                        console.log(userId);
                        console.log(forkrumb.id);
                        resolve();
                    }
                });

            });
        }
        
        var sql = "";
        var params = [];
        
        if (!filterprivate) {
            sql = "exec krumb.getByLocation @userid=?, @longitude=?, @latitude=?, @distancemeters=?, @skip=?, @take=?, @beaconids=?";
            params = [userId, longitude, latitude, 400, skip, take, beaconids];
        }
        else {
            sql = "exec krumb.getByLocationPrivate @userid=?, @longitude=?, @latitude=?, @distancemeters=?, @skip=?, @take=?, @beaconids=?";
            params = [userId, longitude, latitude, 400, 0, 100, beaconids];
        }
        
        request.service.mssql.query(sql, params, {
            success: function (results) {
                
                var radius = 0;
                if (results.length > 0)
                    radius = results[results.length - 1].radius;
                
                result.radius = Math.ceil(radius);
                result.clusterradius = Math.ceil(radius * 2);
                
                if (results.length > 0) {
                    
                    var total = results.length;
                    var count = 0;
                    
                    results.forEach(function (krumbresult) {
                        var krumb = {};
                        krumb.weight = calculateWeight(krumbresult, radius);
                        krumb.id = krumbresult.id;
                        krumb.userid = krumbresult.userid;
                        krumb.username = krumbresult.username;
                        krumb.text = krumbresult.text;
                        krumb.image = krumbresult.image;
                        krumb.drawing = krumbresult.drawing;
                        krumb.numberlikes = krumbresult.numberlikes;
                        krumb.numbercomments = krumbresult.numbercomments;
                        krumb.mylike = krumbresult.mylike;
                        krumb.following = krumbresult.following;
                        krumb.beaconid = krumbresult.beaconid;
                        krumb.distance = krumbresult.radius;
                        krumb.createdAt = krumbresult.createdAt.getTime();
                        
                        if (krumbresult.validuntil)
                            krumb.validuntil = krumbresult.validuntil.getTime();
                        else
                            krumb.validuntil = 0;
                        
                        krumb.location = {};
                        krumb.location.longitude = krumbresult.longitude;
                        krumb.location.latitude = krumbresult.latitude;
                        krumb.location.name = krumbresult.name;
                        krumb.location.id = krumbresult.location;
                        krumb.location.beaconid = krumbresult.beaconid;
                        krumb.comments = [];
                        var nlength = result.krumbs.push(krumb);
                        
                        if (krumb.numbercomments > 0)
                            asyncTasks.push(commentspromise(krumb));

                    });
                    
                    var chain = Promise.resolve();
                    
                    if (asyncTasks.length > 0) {
                        chain = asyncTasks.reduce(function (previous, item) {
                            
                            return previous.then(function () {
                                return item;
                            });

                        }, Promise.resolve());
                    }
                    
                    chain.then(function () {
                        
                        if (!filterprivate) {
                            var sql = "exec krumb.clusterKrumbs @longitude=?, @latitude=?, @innerdistancemeters=?, @outerdistancemeters=?";
                            
                            request.service.mssql.query(sql, [longitude, latitude, result.radius, result.clusterradius], {
                                success: function (cresults) {
                                    
                                    result.outsiders = cresults;
                                    
                                    queuehelper.addServiceBusCommand("commands", "locationset", { userid: userId, lng: longitude, lat: latitude })
										.then(function () {
                                        request.respond(200, result);
                                    });
                                },
                                error: function (err) {
                                    console.log("db error: " + err);
                                    request.respond(500, { code: 500, message: "db error" });
                                }
                            });
                        } else {
                            
                            result.outsiders = [];
                            
                            queuehelper.addServiceBusCommand("commands", "locationset", { userid: userId, lng: longitude, lat: latitude })
								.then(function () {
                                request.respond(200, result);
                            });
						
                        }
                    });
                }
                else {
                    result.radius = 0;
                    result.clusterradius = 0;
                    result.outsiders = [];
                    result.krumbs = [];
                    
                    request.respond(200, result);
                }
            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, "db error");
            }
        });

    });
    
    api.get('/listv4', function (request, response) {
        var userId = request.user.userId;
        
        var longitude = parseFloat(request.param("longitude", "0.0"));
        var latitude = parseFloat(request.param("latitude", "0.0"));
        var beaconids = request.param("beaconids", "");
        var filteruserid = request.param("userid", "");
        
        var beaconweights = {};
        if (beaconids.length > 0) {
            
            var parts = beaconids.split(",");
            
            for (var i = 0; i < parts.length; i++) {
                
                beaconweights["id" + parts[i]] = parts.length - i;
                
            }
            
        }
        
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "100"));
        
        var filterprivate = (request.param("filterprivate", "false") == "true");
        
        if (take > 1000) {
            take = 1000;
        }
        
        var result = {};
        result.krumbs = [];
        var asyncTasks = [];
        
        var calculateWeight = function (krumb, radius) {
            
            var baseweightdistance = 10000;
            var weight = krumb.weight;
            
            //calculate distance
            weight += baseweightdistance * (radius - krumb.radius) / radius;
            
            //if (krumb.userid == userId)
            //    weight += 0;
            //if (krumb.following == 1)
            //    weight += 4000;
            
            if (krumb.beaconid) {
                if (beaconweights.hasOwnProperty("id" + krumb.beaconid)) {
                    weight += 30000 * beaconweights["id" + krumb.beaconid];
                }
            }
            
            return Math.ceil(weight);
			
        }
        
        var commentspromise = function (forkrumb) {
            return new Promise(function (resolve, reject) {
                
                var sql = "exec krumb.getKrumbComments @userid=?, @krumbid=?, @skip=?, @take=?";
                
                request.service.mssql.query(sql, [userId, forkrumb.id, 0, 20], {
                    success: function (cresults) {
                        
                        if (cresults.length > 0) {
                            
                            cresults.forEach(function (cresult) {
                                var comment = {};
                                
                                comment.id = cresult.id;
                                comment.userid = cresult.userid;
                                comment.username = cresult.username;
                                comment.text = cresult.text;
                                comment.image = cresult.image;
                                comment.drawing = cresult.drawing;
                                comment.numberlikes = cresult.numberlikes;
                                comment.numbercomments = cresult.numbercomments;
                                comment.mylike = cresult.mylike;
                                comment.following = cresult.following;
                                comment.beaconid = cresult.beaconid;
                                comment.location = {};
                                comment.location.longitude = cresult.longitude;
                                comment.location.latitude = cresult.latitude;
                                comment.location.name = cresult.name;
                                comment.location.id = cresult.location;
                                comment.location.beaconid = cresult.beaconid;
                                comment.createdAt = cresult.createdAt.getTime();
                                
                                forkrumb.comments.push(comment);
                            });
                        }
                        
                        resolve();
                    },
                    error: function (err) {
                        console.log("database error while fetching comments listv2: " + err);
                        console.log(userId);
                        console.log(forkrumb.id);
                        resolve();
                    }
                });

            });
        }
        
        var sql = "";
        var params = [];
        
        if (filteruserid.length > 0) {
            
            sql = "exec krumb.getKrumbFeedForUser @userid=?, @filteruserid=?, @longitude=?, @latitude=?, @distancemeters=?, @skip=?, @take=?, @beaconids=?";
            params = [userId, filteruserid, longitude, latitude, 400, skip, take, beaconids];
		
        }
        else {
            if (!filterprivate) {
                sql = "exec krumb.getKrumbFeedPublic @userid=?, @longitude=?, @latitude=?, @distancemeters=?, @skip=?, @take=?, @beaconids=?";
                params = [userId, longitude, latitude, 400, skip, take, beaconids];
            }
            else {
                sql = "exec krumb.getKrumbFeedPersonal @userid=?, @longitude=?, @latitude=?, @distancemeters=?, @skip=?, @take=?, @beaconids=?";
                params = [userId, longitude, latitude, 400, 0, 100, beaconids];
            }
        }
        
        request.service.mssql.query(sql, params, {
            success: function (results) {
                
                var radius = 0;
                if (results.length > 0)
                    radius = results[results.length - 1].radius;
                
                result.radius = Math.ceil(radius);
                result.clusterradius = Math.ceil(radius * 2);
                
                if (results.length > 0) {
                    
                    var total = results.length;
                    var count = 0;
                    
                    results.forEach(function (krumbresult) {
                        var krumb = {};
                        krumb.weight = calculateWeight(krumbresult, radius);
                        krumb.id = krumbresult.id;
                        krumb.userid = krumbresult.userid;
                        krumb.username = krumbresult.username;
                        krumb.text = krumbresult.text;
                        krumb.image = krumbresult.image;
                        krumb.drawing = krumbresult.drawing;
                        krumb.numberlikes = krumbresult.numberlikes;
                        krumb.numbercomments = krumbresult.numbercomments;
                        krumb.mylike = krumbresult.mylike;
                        krumb.following = krumbresult.following;
                        krumb.beaconid = krumbresult.beaconid;
                        krumb.distance = krumbresult.radius;
                        krumb.createdAt = krumbresult.createdAt.getTime();
                        
                        if (krumbresult.validuntil)
                            krumb.validuntil = krumbresult.validuntil.getTime();
                        else
                            krumb.validuntil = 0;
                        
                        krumb.location = {};
                        krumb.location.longitude = krumbresult.longitude;
                        krumb.location.latitude = krumbresult.latitude;
                        krumb.location.name = krumbresult.name;
                        krumb.location.id = krumbresult.location;
                        krumb.location.beaconid = krumbresult.beaconid;
                        krumb.visibility = krumbresult.visibility;
                        krumb.comments = [];
                        var nlength = result.krumbs.push(krumb);
                        
                        if (krumb.numbercomments > 0)
                            asyncTasks.push(commentspromise(krumb));

                    });
                    
                    var chain = Promise.resolve();
                    
                    if (asyncTasks.length > 0) {
                        chain = asyncTasks.reduce(function (previous, item) {
                            
                            return previous.then(function () {
                                return item;
                            });

                        }, Promise.resolve());
                    }
                    
                    chain.then(function () {
                        
                        if (!filterprivate) {
                            var sql = "exec krumb.clusterKrumbs @longitude=?, @latitude=?, @innerdistancemeters=?, @outerdistancemeters=?";
                            
                            request.service.mssql.query(sql, [longitude, latitude, result.radius, result.clusterradius], {
                                success: function (cresults) {
                                    
                                    result.outsiders = cresults;
                                    
                                    queuehelper.addServiceBusCommand("commands", "locationset", { userid: userId, lng: longitude, lat: latitude })
										.then(function () {
                                        request.respond(200, result);
                                    });
                                },
                                error: function (err) {
                                    console.log("db error: " + err);
                                    request.respond(500, { code: 500, message: "db error" });
                                }
                            });
                        } else {
                            
                            result.outsiders = [];
                            
                            queuehelper.addServiceBusCommand("commands", "locationset", { userid: userId, lng: longitude, lat: latitude })
								.then(function () {
                                request.respond(200, result);
                            });
						
                        }
                    });
                }
                else {
                    result.radius = 0;
                    result.clusterradius = 0;
                    result.outsiders = [];
                    result.krumbs = [];
                    
                    request.respond(200, result);
                }
            },
            error: function (err) {
                console.log("db error: " + err);
                request.respond(500, "db error");
            }
        });

    });
    
    api.get('/krumbdetail', function (request, response) {
        var userId = request.user.userId;
        
        var krumbid = request.param("krumbid");
        
        var asyncTasks = [];
        
        var commentspromise = function (forkrumb) {
            return new Promise(function (resolve, reject) {
                
                if (forkrumb.numbercomments > 0) {
                    var sql = "exec krumb.getKrumbComments @userid=?, @krumbid=?, @skip=?, @take=?";
                    
                    request.service.mssql.query(sql, [userId, forkrumb.id, 0, 20], {
                        success: function (cresults) {
                            
                            if (cresults.length > 0) {
                                
                                cresults.forEach(function (cresult) {
                                    var comment = {};
                                    
                                    comment.id = cresult.id;
                                    comment.userid = cresult.userid;
                                    comment.username = cresult.username;
                                    comment.text = cresult.text;
                                    comment.image = cresult.image;
                                    comment.drawing = cresult.drawing;
                                    comment.location = {};
                                    comment.location.longitude = cresult.longitude;
                                    comment.location.latitude = cresult.latitude;
                                    comment.location.name = cresult.name;
                                    comment.createdAt = cresult.createdAt.getTime();
                                    
                                    forkrumb.comments.push(comment);
                                });
                            }
                            
                            resolve();
                        },
                        error: function (err) {
                            console.log("database error while fetching comments: " + err);
                            resolve();
                        }
                    });
                }
                else {
                    resolve();
                }

            });
        }
        
        function getFollowers(userid) {
            var promisefollowers = new Promise(function (resolve, reject) {
                
                request.service.mssql.query("exec krumb.getFollowList @userid=?, @fulllist=?", 
                    [userid, 0], {
                    success: function (results) {
                        
                        resolve(results);

                    },
                    error: function (err) {
                        console.log("database error: " + err);
                        
                        reject(err);
                    }
                });
            });
            
            return promisefollowers;
        }        ;
        
        function getLikes(krumbid) {
            
            var promiselikes = new Promise(function (resolve, reject) {
                
                request.service.mssql.query("exec krumb.getLastKrumbLikes @krumbid=?", 
                    [krumbid], {
                    success: function (results) {
                        
                        resolve(results);

                    },
                    error: function (err) {
                        console.log("database error: " + err);
                        
                        reject(err);
                    }
                });
				
            });
            
            return promiselikes;
        }        ;
        
        getLikes(krumbid).then(function (likes) {
            var sql = "exec krumb.getByKrumbId @userid=?, @krumbid=?";
            
            request.service.mssql.query(sql, [userId, krumbid], {
                success: function (results) {
                    
                    if (results.length > 0) {
                        
                        var krumb = {};
                        
                        var result = results[0];
                        
                        getFollowers(result.userid).then(function (resultsfollowers) {
                            krumb.weight = result.weight / 16000;
                            krumb.id = result.id;
                            krumb.userid = result.userid;
                            krumb.username = result.username;
                            krumb.text = result.text;
                            krumb.image = result.image;
                            krumb.drawing = result.drawing;
                            krumb.numberlikes = result.numberlikes;
                            krumb.numbercomments = result.numbercomments;
                            krumb.mylike = result.mylike;
                            krumb.distance = result.radius;
                            krumb.createdAt = result.createdAt.getTime();
                            
                            if (result.validuntil)
                                krumb.validuntil = result.validuntil.getTime();
                            else
                                krumb.validuntil = 0;
                            
                            krumb.location = {};
                            krumb.location.longitude = result.longitude;
                            krumb.location.latitude = result.latitude;
                            krumb.location.name = result.name;
                            krumb.isprivate = result.visibility > 0;
                            krumb.visibility = result.visibility;
                            
                            krumb.usercounts = {};
                            krumb.usercounts.followingcount = resultsfollowers[0].countfollowing;
                            krumb.usercounts.followercount = resultsfollowers[0].countfollower;
                            krumb.usercounts.krumbcount = resultsfollowers[0].countkrumb;
                            
                            krumb.likes = likes;
                            
                            krumb.comments = [];
                            
                            asyncTasks.push(commentspromise(krumb));
                            
                            var chain = asyncTasks.reduce(function (previous, item) {
                                
                                return previous.then(function () {
                                    return item;
                                });

                            }, Promise.resolve());
                            
                            chain.then(function () {
                                
                                request.respond(200, krumb);

                            });
                        });
                    }
                    else {
                        request.respond(404, { code: 404, message: "This krumb does not exist or has been deleted" });
                    }
                },
                error: function (err) {
                    console.log("db error: " + err);
                    request.respond(500, "db error");
                }
            });
        })
		.catch(function (err) {
            console.log("Promise rejected while fetching krumb detail " + err);
            request.respond(500, "error while fetching krumb detail");
        });
    });
    
    api.post('/attachprivatekrumb', function (request, response) {
        var userId = request.user.userId;
        var krumbid = request.param("krumbid");
        
        if (krumbid.length == 0) {
            request.respond("500", "the krumbid parameter is mandatory");
            return;
        }
        
        var privateTable = request.service.tables.getTable("dbprivatekrumbs");
        var privkrumb = {};
        privkrumb.foruser = userId;
        privkrumb.krumbid = krumbid;
        privateTable.insert(privkrumb, {
            success: function (privresults) {
                request.respond(200);
            },
            error: function (err) {
                console.log("db error: " + err);
                if (err.code == 11000) {
                    request.respond(200);
                }
                else {
                    request.respond(500, "db error");
                }
            }
        });
        
    });
    
    api.get('/downloadInstaMedia', function (request, response) {
        var userId = request.user.userId;
        var socialtoken = request.param("socialtoken");
        var socialhelper = require('./../shared/SocialMediaHelper');
        
        request.respond(200);

        var verifyurl = 'https://api.instagram.com/v1/users/self/media/recent?count=10&access_token=' + socialtoken;
        
        socialhelper.downloadRawInstagramMedia(verifyurl)
        .then(function (media) {
            var len = media.length;
            //console.log("All instagram imagesdata(" + len + ") downloaded " + JSON.stringify(media));
            if (len > 0) {
                Promise.all(media.map(socialhelper.downloadInstagramPicture)).then(function (results) {
                    //console.log("Instagram test, received results " + JSON.stringify(results));
                    var userdbTable = request.service.tables.getTable('dbuser');
                    userdbTable.where({ id: userId }).read({
                        success: function (userresults) {
                            results.forEach(function (krumbresult) {
                                request.params.location_name = krumbresult.location_name;
                                request.params.location_long = krumbresult.location_longitude;
                                request.params.location_lat = krumbresult.location_latitude;
                                locationhelper.getLocation(request, response)
                                .then(function (location) {
                                    var validity = 0;
                                    
                                    var krumb = {};
                                    krumb.text = krumbresult.text;
                                    krumb.userid = userresults[0].id;
                                    krumb.username = userresults[0].username;
                                    krumb.heading = 0;
                                    krumb.location = location.id;
                                    krumb.image = krumbresult.imagename;
                                    krumb.drawing = "";
                                    krumb.backgroundColor = "";
                                    krumb.ip = "";
                                    krumb.longitude = location.longitude;
                                    krumb.latitude = location.latitude;
                                    krumb.locationname = location.name;
                                    if (typeof location.beaconid !== 'undefined') {
                                        krumb.beaconid = location.beaconid;
                                    }
                                    else {
                                        krumb.beaconid = null;
                                    }
                                    krumb.refid = null;
                                    krumb.validuntil = null;
                                    
                                    var krumbTable = request.service.tables.getTable("dbkrumb");
                                    krumbTable.insert(krumb, {
                                        success: function (insertresults) {
                                            queuehelper.addServiceBusCommand("commands", "krumbadded", { userid: userId, krumbid: insertresults.id });
                                            //request.respond(200);
                                        },
                                        error: function (err) {
                                            console.log("Create krumb from instagram failed: " + err);
                                            //request.respond(500, err);
                                        }
                                    });
                                }).catch(function (err) {
                                    console.log("Create krumb from instagram location not found " + err);
                                    //request.respond(500, err);
                                });
                            });
                        },
                        error: function (err) {
                            console.log("Cannot find user " + err);
                            //request.respond(500, err);
                        }
                    });
                });

            } else {
                console.log("krumb.js InstaDownload no results " + krumbresults);
                //request.respond(200);
            }
        }).catch(function (err) {
            console.log(err);
            //request.respond(500, err);
        });
    });
};
var comhelper = require("./../shared/CommunicationHelper");
var Promise = require("es6-promise").Promise;

exports.register = function(api) {

    api.post('/sendinvitationsms', function(request, response) {
        
        var tonumber = request.param("to", "");
        
        if (tonumber.length == 0) {
            request.respond("500", { message: "to is a mandatory parameter"} );
            return;
        }
        
        var tofriendly = request.param("tofriendly", "");
        
        if (tofriendly.length == 0) {
            request.respond("500", { code: 500, message: "tofriendly is a mandatory parameter"} );
            return;
        }
        
    });
    
    api.post('/sendinvitationmail', function(request, response) {
        
        var tomail = request.param("to", "");
        var userId = request.user.userId;
        
        if (tomail.length == 0) {
            request.respond("500", { message: "to is a mandatory parameter"} );
            return;
        }
        
        var userdbTable = request.service.tables.getTable("dbuser");
        userdbTable.where({ id: userId }).read({
            success: function (results) {
        
                comhelper.sendEmail(tomail, "no_reply@krumb.be", results[0].username, results[0].username, "2ae4c72a-3475-43b7-b86a-8ecfce81dd0f", { name : results[0].username }).then(function() {
        			
        			request.respond(200, { success: "true" });
        			
        		}, function(err) {
        			
        			console.log("sending mail error: " + err);
        			request.respond(500, { code: 500, message: "error while sending mail" });
        			
        		});
            },
            error: function(err) {
                
                console.log("database error in sendinvitationmail: " + err);
                request.respond(500, { code: 500, message: "database error" });
                
            }
        });
        
    });
    
    api.post('/reportissue', function(request, response) {
        
        var userid = request.user.userId;
        var issue = request.param("issue", "");
        
        if (issue.length == 0) {
            request.respond("500", { code: 500, message: "issue is a mandatory parameter"} );
            return;
        }
		
		var category = request.param("category", "");
        
        if (category.length == 0) {
            request.respond("500", { code: 500, message: "category is a mandatory parameter"} );
            return;
        }
        
        var issueTable = request.service.tables.getTable("dbissue");
        issueTable.insert({
            userid: userid,
            issue: issue,
            category: category
        }, {
            success: function(result) {
                
                request.respond(200, { success : true });
                
            },
            error: function(err) {
                
                console.log("database error in reportissue: " + err);
                request.respond(500, { code: 500, message: "database error" });
                
            }
        });
        
        
    });
    
};
var queuehelper = require('./../shared/QueueHelper');

var distanceEnum = {};
distanceEnum.far = 5000;
distanceEnum.close = 1000;
distanceEnum.nearby = 200;
distanceEnum.at = 50;

exports.register = function(api) {
    
    api.post('/setpushlocation', function(request, response) {
        
        console.log("called setlocation");
        var userId = request.user.userId;
        var longitude = request.param("longitude", "0.0");
        var latitude = request.param("latitude", "0.0");
        
        queuehelper.addServiceBusCommand("commands", "locationset", { userid: userId, lng: longitude, lat: latitude })
				.then(function() {
                    request.respond(200, { success: true });
				});
        
        
        
    });
    
    api.get('/findbybeaconid', function(request, response) {
        var beaconid = request.param("beaconid", "");
        
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        if (take > 50) {
            take = 50;
        }
        
        var sql = "exec GetValidBeacons @beaconids=?";
        
        request.service.mssql.query(sql, [beaconid], {
        
            success: function(results) {
                request.respond(200, results);
            },
            error: function(err) {
                console.log("db error: " + err);
                request.respond(500, { code: 500, message: "db error" });
            }
        });
    });
    
    api.get('/find', function(request, response) {
        var distance = request.param("distance", "nearby");
        var distancemeters = 50;
        
        if (distanceEnum.hasOwnProperty(distance))
            distancemeters = distanceEnum[distance];
        else
            distancemeters = distanceEnum.far;
            
        var longitude = request.param("longitude", null);
        var latitude = request.param("latitude", null);
        
        var beaconids = request.param("beaconids", "");
        
        var skip = parseInt(request.param("skip", "0"));
        var take = parseInt(request.param("take", "20"));
        
        if (take > 50) {
            take = 50;
        }
        
        var sql = "";
        var params = [];
        if (longitude != null && latitude != null) {
            longitude = parseFloat(longitude);
            latitude = parseFloat(latitude);
        
            sql = "exec GetCloseLocations @longitude = ?, @latitude = ?, @distancemeters = ?, @skip = ?, @take = ?, @beaconids = ?";
            params = [longitude, latitude, distancemeters, skip, take, beaconids];
        }
        else {
            sql = "exec GetCloseLocations @distancemeters = ?, @skip = ?, @take = ?, @beaconids = ?";
            params = [distancemeters, skip, take, beaconids];
        }
            
        
        request.service.mssql.query(sql, params, {
            success: function(results) {
                request.respond(200, results);
            },
            error: function(err) {
                console.log("db error: " + err);
                request.respond(500, { code: 500, message: "db error" });
            }
        });
    });
    
    api.get('/view', function(request, response) {
        
        var locationid = request.param("id", 0);
        
        var dblocationTable = request.service.tables.getTable('dblocation');
        dblocationTable.select(function() {
            return {
                "id": this.id,
                "name": this.name,
                "longitude": this.longitude,
                "latitude": this.latitude
            };
        }).where({ id : locationid}).read({
           success: function(results) {
               if (results.length > 0)
                request.respond(200, results[0]);
               else
                request.respond(404, { code: 404, message : "Not found" });
           } 
        });
        
        
    });
};
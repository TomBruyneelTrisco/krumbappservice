var authhelper = require('./../shared/AuthenticationHelper');
var socialmedia = require('./../shared/SocialMediaHelper');
var comhelper = require('./../shared/CommunicationHelper');
var queuehelper = require('./../shared/QueueHelper');
var docdb = require('./../shared/DocumentDbHelper');

exports.register = function (api) {
    

    api.post('/validateinvitecode', function (request, response) {
        console.log("validateinvitecode called");

        var invitecode = request.param("code", "");
        if (invitecode.length == 0) {
            request.respond(500, { code: 500, message: "code is mandatory" });
            return;
        }

        console.log("trying to validate code: " + invitecode);

        if (invitecode.toUpperCase() == "UNLOCKPLACES") {
            request.respond(200, { success: true });
            return;
        }

        var dbinviteTable = request.service.tables.getTable('dbinvitecodes');
        dbinviteTable.where({ code: invitecode }).read({
            success: function (results) {

                if (results.length > 0) {
                    var dbcode = results[0];

                    if (dbcode.used == 2) {
                        request.respond(401, { code: 401, message: "code already used" });
                    }
                    else {

                        dbcode.used = 2;
                        dbinviteTable.update(dbcode, {

                            success: function (results) {
                                request.respond(200, { success: true });
                            },
                            error: function (err) {
                                console.log("database error validateinvitecode: " + err);
                                request.respond(500, { code: 500, message: "database error" });
                            }

                        });

                    }
                }
                else {

                    request.respond(401, { code: 401, message: "invalid code" });

                }

            },
            error: function (err) {
                console.log("database error validateinvitecode: " + err);
                request.respond(500, { code: 500, message: "database error" });
            }
        });
    });

    api.get('/validatetoken', function (request, response) {

        var userid = request.param("userid", "");
        if (userid.length == 0) {
            request.respond(500, { code: 500, message: "userid is mandatory" });
            return;
        }

        var token = request.param("token", "");
        if (token.length == 0) {
            request.respond(500, { code: 500, message: "token is mandatory" });
            return;
        }
        //Validate token
        var success = authhelper.validateToken(userid, token, request.service.config.masterKey);
        if (success) {
            request.respond(200, { message: "Validated" });
        }
        else {
            request.respond(404, { code: 404, message: "Invalid token" });
        }
    });

    api.get('/renewtoken', function (request, response) {
        if (request.user.level == "authenticated") {
            var expiry = new Date();
            expiry.setUTCDate(expiry.getUTCDate() + 30);

            var userid = request.user.userId;

            var token = authhelper.zumoJwt(expiry, authhelper.aud, userid, request.service.config.masterKey);

            request.respond(200, { token: token });
        }
        else {
            request.respond(401, { code: 401, message: "request a new token with process" });
        }
    });

    api.get('/passwordretrievaloptions', function (request, response) {
        //Username can be both username or email
        var username = request.param("username", "");

        var dbuserTable = request.service.tables.getTable('dbuser');
        dbuserTable.where(function (loginId) {
            return this.username == loginId || this.email == loginId;
        }, username).read({
            success: function (results) {

                if (results.length > 0) {

                    var dbuser = results[0];

                    var options = {};
                    options.hasmail = dbuser.email != null && dbuser.email.length > 0;
                    options.hasmobile = dbuser.mobilenumber != null && dbuser.mobilenumber.length > 0;


                    request.respond(200, options);
                }
                else {

                    request.respond(404, { code: 404, message: "username or mail not found" });

                }

            },
            error: function (err) {
                console.log("database error passwordretrievaloptions: " + err);
                request.respond(500, { code: 500, message: "database error" });
            }
        });
    });

    api.post('/requestpasswordresetcode', function (request, response) {
        //Username can be both username or email
        var username = request.param("username", "");
        if (username.length == 0) {
            request.respond(500, { code: 500, message: "username or email is mandatory" });
            return;
        }

        var type = request.param("type", "");
        if (type.length == 0) {
            request.respond(500, { code: 500, message: "type is mandatory" });
            return;
        }

        //create code
        var code = authhelper.randomAsciiString(10);

        var dbuserTable = request.service.tables.getTable('dbuser');
        dbuserTable.where(function (loginId) {
            return this.username == loginId || this.email == loginId;
        }, username).read({
            success: function (results) {

                if (results.length > 0) {

                    var dbuser = results[0];
                    var now = (new Date()).getTime();
                    now += 1000 * 60 * 30;

                    var dbpresetTable = request.service.tables.getTable('dbpasswordresetcodes');

                    dbpresetTable.where({ userid: dbuser.id }).read({
                        success: function (results) {

                            if (results.length > 0) {
                                //user already has a password reset token, update
                                
                                var oldtoken = results[0];
                                oldtoken.code = code;
                                oldtoken.attempts = 0;
                                oldtoken.validuntil = (new Date(now));
                                oldtoken.type = type;

                                dbpresetTable.update(oldtoken,
                                    {
                                        success: function (result) {

                                            comhelper.sendEmail(dbuser.email, "no_reply@krumb.be", "", dbuser.username, "8f1a91fa-9fc0-406a-8288-4ba482affb99", { code: code }).then(function () {

                                                request.respond(200, { success: "true" });

                                            }, function (err) {

                                                    console.log("sending mail error: " + err);
                                                    request.respond(500, { code: 500, message: "error while sending mail" });

                                                });

                                        },
                                        error: function (err) {
                                            console.log("database error passwordretrievaloptions: " + err);
                                            request.respond(500, { code: 500, message: "database error" });
                                        }
                                    });

                            }
                            else {
                                
                                //no password reset token yet, add one
                                dbpresetTable.insert({
                                    code: code,
                                    userid: dbuser.id,
                                    validuntil: (new Date(now)),
                                    type: type,
                                    attempts: 0
                                },
                                    {
                                        success: function (result) {

                                            comhelper.sendEmail(dbuser.email, "no_reply@krumb.be", "", dbuser.username, "8f1a91fa-9fc0-406a-8288-4ba482affb99", { code: code }).then(function () {

                                                request.respond(200, { success: "true" });

                                            }, function (err) {

                                                    console.log("sending mail error: " + err);
                                                    request.respond(500, { code: 500, message: "error while sending mail" });

                                                });

                                        },
                                        error: function (err) {
                                            console.log("database error passwordretrievaloptions: " + err);
                                            request.respond(500, { code: 500, message: "database error" });
                                        }
                                    });
                            }


                        },
                        error: function (err) {
                            console.log("database error passwordretrievaloptions: " + err);
                            request.respond(500, { code: 500, message: "database error" });
                        }
                    });

                }
                else {

                    request.respond(404, { code: 404, message: "username not found" });

                }

            },
            error: function (err) {
                console.log("database error passwordretrievaloptions: " + err);
                request.respond(500, { code: 500, message: "database error" });
            }
        });

    });

    api.post('/resetpassword', function (request, respond) {
        //Username can be both username or email
        var username = request.param("username", "");

        if (username.length == 0) {
            request.respond(500, { code: 500, message: "username or email is mandatory" });
            return;
        }

        var code = request.param("code", "");
        if (code.length == 0) {
            request.respond(500, { code: 500, message: "code is mandatory" });
            return;
        }

        var newpassword = request.param("newpassword", "");
        if (newpassword.length < 6) {
            request.respond(500, { code: 500, message: "newpassword is mandatory and should be at least 6 characters long" });
            return;
        }

        var dbuserTable = request.service.tables.getTable('dbuser');
        dbuserTable.where(function (loginId) {
            return this.username == loginId || this.email == loginId;
        }, username).read({
            success: function (results) {

                if (results.length > 0) {

                    var dbuser = results[0];

                    var dbpresetTable = request.service.tables.getTable('dbpasswordresetcodes');
                    dbpresetTable.where({
                        userid: dbuser.id
                    }).read({

                        success: function (results) {

                            if (results.length > 0) {
                                var dbreset = results[0];

                                if (dbreset.attempts == 3) {
                                    request.respond(401, { code: 401, message: "too many attempts with an invalid code" });
                                    return;
                                }

                                if (dbreset.code == code) {
                                    if (dbreset.validuntil.getTime() > (new Date()).getTime()) {

                                        dbuser.salt = authhelper.getSalt();
                                        dbuser.password = newpassword;
                                    
                                        // hash the password
                                        authhelper.hash(dbuser.password, dbuser.salt, function (err, h) {

                                            dbuser.password = h;

                                            dbuserTable.update(dbuser, {
                                                success: function (result) {
                                                    var expiry = new Date();
                                                    expiry.setUTCDate(expiry.getUTCDate() + 30);
                                                    var userId = dbuser.id;
                                                    request.respond(200, {
                                                        username: dbuser.username,
                                                        email: dbuser.email,
                                                        id: dbuser.id,
                                                        token: authhelper.zumoJwt(expiry, authhelper.aud, userId, request.service.config.masterKey),
                                                        expires: expiry.getTime(),
                                                        optin: dbuser.optin
                                                    });
                                                },
                                                error: function (err) {
                                                    console.log("database error passwordreset: " + err);
                                                    request.respond(500, { code: 500, message: "database error" });
                                                }
                                            });


                                        });

                                    }
                                    else {

                                        request.respond(401, { code: 401, message: "code expired" });

                                    }
                                }
                                else {
                                    
                                    //code not the same, add one to the attempts
                                    dbreset.attempts += 1;

                                    dbpresetTable.update(dbreset,
                                        {
                                            success: function (result) {
                                                if (dbreset.attempts < 3)
                                                    request.respond(401, { code: 401, message: "invalid code" });
                                                else
                                                    request.respond(401, { code: 401, message: "too many attempts with an invalid code" });
                                            },
                                            error: function (err) {
                                                console.log("database error password reset: " + err);
                                                request.respond(500, { code: 500, message: "database error" });
                                            }
                                        });

                                }
                            }
                            else {
                                request.respond(404, { code: 404, message: "code not found" });
                            }

                        },
                        error: function (err) {
                            console.log("database error passwordreset: " + err);
                            request.respond(500, { code: 500, message: "database error" });
                        }

                    });

                }
                else {

                    request.respond(404, { code: 404, message: "username not found" });

                }

            },
            error: function (err) {
                console.log("database error passwordreset: " + err);
                request.respond(500, { code: 500, message: "database error" });
            }
        });



    });

    api.post('/processSocialUser', function (request, response) {

        var socialMediaType = request.param("socialMediaType", "FB");
        var socialMediaId = request.param("socialMediaId", "");
        var socialMediaToken = request.param("socialMediaToken", "");
        var socialMediaVerifyUrl = request.param("socialMediaVerifyUrl", "https://api.twitter.com/1.1/account/verify_credentials.json");

        socialmedia.checkUserLogin(socialMediaType, socialMediaId, socialMediaToken, socialMediaVerifyUrl)
            .then(function (loginsucceeded) {
            if (loginsucceeded) {

                var combinedId = socialMediaType + socialMediaId;

                var dbuserTable = request.service.tables.getTable('dbuser');
                dbuserTable.where({ socialMediaId: combinedId }).read({
                    success: function (results) {
                        if (results.length > 0) {
                            var dbuser = results[0];

                            var expiry = new Date();
                            expiry.setUTCDate(expiry.getUTCDate() + 30);
                            var userId = dbuser.id;
                            request.respond(200, {
                                username: dbuser.username,
                                email: dbuser.email,
                                id: dbuser.id,
                                token: authhelper.zumoJwt(expiry, authhelper.aud, userId, request.service.config.masterKey),
                                expires: expiry.getTime(),
                                optin: dbuser.optin
                            });
                        }
                        else {
                            request.respond(404, {
                                code: 404,
                                "message": "user not found"
                            });
                        }
                    },
                    error: function (err) {
                        request.respond(500, {
                            code: 500,
                            "message": "database error"
                        });
                    }
                });
            }
            else {
                request.respond(500, {
                    code: 500,
                    "message": "invalid accesstoken"
                });
            }
        });

    });

    api.post('/process', function (request, response) {
        //Username can be both username or email
        var username = request.param("username", "");

        var dbuserTable = request.service.tables.getTable('dbuser');
        dbuserTable.where(function (loginId) {
            return this.username == loginId || this.email == loginId;
        }, username).read({
            success: function (results) {

                if (results.length == 0) {
                    request.respond(401, {
                        code: 401,
                        message: "Incorrect username or password"
                    });
                }
                else {
                    var dbuser = results[0];
                    authhelper.hash(request.param('password'), dbuser.salt, function (err, h) {
                        var incoming = h;
                        if (authhelper.slowEquals(incoming, dbuser.password)) {
                            var expiry = new Date();
                            expiry.setUTCDate(expiry.getUTCDate() + 30);
                            var userId = dbuser.id;
                            request.respond(200, {
                                username: dbuser.username,
                                email: dbuser.email,
                                id: dbuser.id,
                                token: authhelper.zumoJwt(expiry, authhelper.aud, userId, request.service.config.masterKey),
                                expires: expiry.getTime(),
                                optin: dbuser.optin
                            });

                            var dbname = docdb.getDatabaseName();
                            var client = docdb.getClient();

                            docdb.readOrCreateDatabase(client, dbname).then(function (database) {
                                return docdb.readOrCreateCollection(client, database, "authlogs");
                            }).then(function (collection) {
                                docdb.insertDocument(client, collection, { userid: dbuser.id, time: new Date(), ip: request.headers['x-forwarded-for'], success: true });
                            });
                        }
                        else {
                            request.respond(401, {
                                code: 401,
                                message: "Incorrect username or password"
                            });

                            docdb.readOrCreateDatabase(client, dbname).then(function (database) {
                                return docdb.readOrCreateCollection(client, database, "authlogs");
                            }).then(function (collection) {
                                docdb.insertDocument(client, collection, { userid: dbuser.id, time: new Date(), ip: request.headers['x-forwarded-for'], success: false });
                            });
                        }
                    });
                }
            },
            error: function (err) {
                console.log("Database error bij auth process: " + err);
                request.respond(500, {
                    code: 500,
                    message: "An error has accured"
                });
            }
        });

    });
};
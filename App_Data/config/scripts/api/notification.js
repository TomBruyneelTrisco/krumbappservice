exports.register = function(api) {
    api.get('/list', function(request, response) {

        var MongoClient = require('mongodb').MongoClient;
        var userid = request.user.userId;
        
        var skip = parseInt(request.param("skip", 0));
        var take = parseInt(request.param("take", 20));
		
		//filters
		var filter = request.param("filter", "none");
		var queryparams = {};
		queryparams.ForUser = { $in: [userid] };
		
		if (filter == "here" || filter == "elsewhere") {
			var longitude = parseFloat(request.param("long", 0.0));
			var latitude = parseFloat(request.param("lat", 0.0));
			
			var location = {};
			
			if (filter == "here") {
				location = 	{ $near: {
								$geometry: {
									type: "Point" ,
									coordinates: [ longitude, latitude ]
								},										
								$maxDistance: 400,
								$minDistance: 0
							} };
			}
			else {
				location = 	{ $near: {
								$geometry: {
									type: "Point" ,
									coordinates: [ longitude, latitude ]
								},
								$minDistance: 400
							} };
			}
									
			
			queryparams.Location = location;
		}
		else if (filter == "userrelations") {
			
			queryparams.Type = { $in: [3] };
			
		}

        // Connection URL
        var url = 'mongodb://krumb:Krum8s_mongo@ds064117-a0.mongolab.com:64117,ds064117-a1.mongolab.com:64116/krumb';
        

        // Use connect method to connect to the Server
        MongoClient.connect(url, function(err, db) {

            if (!err) {
                var collection = db.collection('notifications');
                
                collection.find(queryparams, { skip : skip, limit : take, sort : { Added : -1 }}).toArray(function(err, docs) {
                
                    var ids = [];
                    for (var i = 0; i < docs.length; i++) {
                        ids.push(docs[i]._id);
                    }
                    
                    request.respond(200, docs);
                    
                    collection.update({_id:{$in:ids}}, { $set: { Read: true } }, { multi: true }, function(err, result) {
                    
                        db.close();
                    });
                
                });      
                
                
            }
            else {
                console.log(err);
                request.respond(500, { code: 500, message: "Couldn't connect to database" });
            }


        });
    });
    
    api.get('/countunread', function(request, response) {
        var MongoClient = require('mongodb').MongoClient;
        var userid = request.user.userId;
		
		//filters
		var filter = request.param("filter", "none");
		var queryparams = {};
		queryparams.ForUser = { $in: [userid] };
		
		if (filter == "here" || filter == "elsewhere") {
			var longitude = parseFloat(request.param("long", 0.0));
			var latitude = parseFloat(request.param("lat", 0.0));
			
			var location = {};
			
			if (filter == "here") {
				location = 	{ $near: {
								$geometry: {
									type: "Point" ,
									coordinates: [ longitude, latitude ]
								},										
								$maxDistance: 400,
								$minDistance: 0
							} };
			}
			else {
				location = 	{ $near: {
								$geometry: {
									type: "Point" ,
									coordinates: [ longitude, latitude ]
								},
								$minDistance: 400
							} };
			}
									
			
			queryparams.Location = location;
		}
		else if (filter == "userrelations") {
			
			queryparams.Type = { $in: [3] };
			
		}
		
		queryparams.Read = false;
        
        var url = 'mongodb://krumb:Krum8s_mongo@ds064117-a0.mongolab.com:64117,ds064117-a1.mongolab.com:64116/krumb';
        
        MongoClient.connect(url, function(err, db) {
            if(!err) {
                
                var collection = db.collection('notifications');
                collection.count(queryparams, function(err, count) {
                    
                    if(!err){
					
						request.respond(200, {'count':count});
                        
						db.close();
                    }
                    else{
                        console.log(err);
                        request.respond(500, { code: 500, message: "The query failed"});
                    }
                });
            }
            else {
                console.log(err);
                request.respond(500, { code: 500, message: "Couldn't connect to database"});
            }
        });
    });
       
    api.post('/resetbadgenumber', function(request, response) {

        var MongoClient = require('mongodb').MongoClient;
        var ObjectID = require('mongodb').ObjectID;
        var userid = request.user.userId;
        
        // Connection URL
        var url = 'mongodb://krumb:Krum8s_mongo@ds064117-a0.mongolab.com:64117,ds064117-a1.mongolab.com:64116/krumb';
        

        // Use connect method to connect to the Server
        MongoClient.connect(url, function(err, db) {

            if (!err) {
                var collection = db.collection('notifications');
                
                var filters = {};
                filters = { ForUser: { $in: [userid] } };
                    
                filters["OnBadge"] = true;
                
                collection.update(filters, { $set: { OnBadge: false } }, { multi: true }, function(err, result) {
                
                    if (!err) {
                        request.respond(200, { success: true });
                    }
                    else {
                        console.log(err);
                        request.respond(500, { code: 500, message: "Db error" });
                    }    
                    
                    db.close();
                });
                
            }
            else {
                request.respond(500, { code: 500, message: "Couldn't connect to database" });
            }


        });
    });
    
    api.post('/read', function(request, response) {
        
        request.respond(200, { success: true });
/*
        var MongoClient = require('mongodb').MongoClient;
        var ObjectID = require('mongodb').ObjectID;
        var userid = request.user.userId;
        
        var notid = request.param("notid", "");

        // Connection URL
        var url = 'mongodb://krumb:Krum8s_mongo@ds064117-a0.mongolab.com:64117,ds064117-a1.mongolab.com:64116/krumb';
        

        // Use connect method to connect to the Server
        MongoClient.connect(url, function(err, db) {

            if (!err) {
                var collection = db.collection('notifications');
                
                var filters = {};
                if (notid.length > 0)
                    filters = { "_id": { $in: [ ObjectID(notid) ] } };
                else
                    filters = { ForUser: { $in: [userid] } };
                    
                filters["Read"] = false;
                
                collection.update(filters, { $set: { Read: true } }, { multi: true }, function(err, result) {
                
                    if (!err) {
                        request.respond(200, { success: true });
                    }
                    else {
                        console.log(err);
                        request.respond(500, { code: 500, message: "Db error" });
                    }    
                    
                    db.close();
                });
                
            }
            else {
                request.respond(500, { code: 500, message: "Couldn't connect to database" });
            }


        });*/
    });
}